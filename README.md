# README #

GPS-based Timelapse SNS Project

### Implementation Plans ###
* Camera
	* General Picture
	* Timelapse
	* Moving Picture (GIF)
* GPS
	* Map tab
		* filter
			* by time slider
			* by whether following
			* by whether followed
		* pinpoint
			* post by clicking pinpoint
			* select adjacent pinpoint to append image
    * Time tab*
    	* filter
    * Notify when approaching to post
    	* filter
    		* by whether following
    		* by whether followed
    	* Using background service. The key is to balance between accuracy and battery optimization
    	
* Server (Firebase)
	* Auth
		* Anonymous, before signing in
		* OAuth2
	* Post
	* Modify
	* Follow
	* Like
	* Search by username or user id
	* Group*

\* is optional.
