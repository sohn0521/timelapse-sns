package kr.ac.mju.cs.timelapsesns.service;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;

public class GpsTrackService extends Service
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = GpsTrackService.class.getSimpleName();

    private GoogleApiClient apiClient;
    private LocationListener listener;

    @Override
    public void onCreate() {
        super.onCreate();

        apiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Intent intent = new Intent(BasicInfo.ACTION_MY_LOCATION);
                intent.putExtra(BasicInfo.EXTRA_LAT_LOCATION, location.getLatitude());
                intent.putExtra(BasicInfo.EXTRA_LNG_LOCATION, location.getLongitude());
                sendBroadcast(intent);
                Log.i(TAG, "lat: " + location.getLatitude() + ", lng: " + location.getLongitude());
            }
        };
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        apiClient.connect();

        return START_NOT_STICKY;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        LocationServices.FusedLocationApi.removeLocationUpdates(apiClient, listener);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.w(TAG, "Connection failed: " + connectionResult.getErrorMessage());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        apiClient.disconnect();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(BasicInfo.ACTION_MY_LOCATION);
            sendBroadcast(intent);

            return;
        }

        // 업데이트 요청을 설정
        LocationRequest request = new LocationRequest();
        request.setFastestInterval(7000);
        request.setInterval(7000);
        request.setSmallestDisplacement(5);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // 리스너 등록
        LocationServices.FusedLocationApi.requestLocationUpdates(apiClient,request, listener);
    }
}
