package kr.ac.mju.cs.timelapsesns.viewholder;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.model.ParcelableUser;

/**
 * Created by Tak on 2017. 12. 28..
 */

public class UserViewHolder extends RecyclerView.ViewHolder
        implements Checkable {

    @BindView(R.id.user_name)
    TextView userName;

    @BindView(R.id.image_check)
    ImageView imageCheck;

    public UserViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null)
                    listener.onUserClicked(view, user, getAdapterPosition());
            }
        });
    }

    private ParcelableUser user;
    public void bind(ParcelableUser user) {
        this.user = user;
        this.userName.setText(user.username);
    }

    private boolean isChecked;
    @Override
    public void setChecked(boolean checked) {
        Log.d("checked", isChecked +"");
        Log.d("setChecked", checked + "");
        if(isChecked != checked) {
            isChecked = checked;
            drawCheck();
        }
    }

    private void drawCheck() {
        if (isChecked) {
            imageCheck.setImageResource(android.R.drawable.checkbox_on_background);
        } else {
            imageCheck.setImageResource(android.R.drawable.checkbox_off_background);
        }
    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public void toggle() {
        setChecked(!isChecked);
    }

    public interface OnUserItemClickListener {
        public void onUserClicked(View view, ParcelableUser user, int position);
    }

    OnUserItemClickListener listener;
    public void setOnUserItemClickListener(OnUserItemClickListener listener) {
        this.listener = listener;
    }
}
