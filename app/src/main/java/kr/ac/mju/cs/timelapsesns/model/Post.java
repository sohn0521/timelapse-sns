package kr.ac.mju.cs.timelapsesns.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({
        "UnusedDeclaration",
        "WeakerAccess Unused"
})
@IgnoreExtraProperties
public class Post {

    // 포스트 데이터
    public String uid;
    public Long postedAt;
    public String author;
    public String title;
    public String body;
    public List<String> privacy;
    public int likeCount = 0;
    public Map<String, Boolean> likedBy = new HashMap<>();

    // 사진 메타데이터
    public String photoGroupUri;
    public List<String> filenames;
    public Integer numPics;

    // 공유설정(선택)
    public List<String> selectedUserNames;
    public List<String> selectedUserFCMs;
    public int year;
    public int month;
    public int day;
    public int hourOfDay;
    public int minute;

    public Post() {}

    public Post(String uid, String author, String title, String body, List<String> privacy, String photoGroupUri, List<String> filenames, int numPics,
                List<String> selectedUserNames, List<String> selectedUserFCMs, int year, int month, int day, int hourOfDay, int minute) {
        this.uid = uid;
        this.postedAt = System.currentTimeMillis();
        this.author = author;
        this.title = title;
        this.body = body;
        this.privacy = privacy;

        this.photoGroupUri = photoGroupUri;
        this.filenames = filenames;
        this.numPics = numPics;

        this.selectedUserNames = selectedUserNames;
        this.selectedUserFCMs = selectedUserFCMs;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hourOfDay = hourOfDay;
        this.minute = minute;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("postedAt", postedAt);
        result.put("author", author);
        result.put("title", title);
        result.put("body", body);
        result.put("privacy", privacy);
        result.put("likeCount", likeCount);
        result.put("likedBy", likedBy);

        result.put("photoGroupUri", photoGroupUri);
        result.put("filenames", filenames);
        result.put("numPics", numPics);

        result.put("selectedUserNames", selectedUserNames);
        result.put("selectedUserFCMs", selectedUserFCMs);
        result.put("year", year);
        result.put("month", month);
        result.put("day", day);
        result.put("hourOfDay", hourOfDay);
        result.put("minute", minute);

        return result;
    }
}
