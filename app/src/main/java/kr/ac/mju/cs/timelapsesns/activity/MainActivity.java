package kr.ac.mju.cs.timelapsesns.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import net.danlew.android.joda.JodaTimeAndroid;

import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.activity.login.FacebookLoginActivity;
import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;
import kr.ac.mju.cs.timelapsesns.fragment.MyPostsFragment;
import kr.ac.mju.cs.timelapsesns.fragment.RecentPostsFragment;
import kr.ac.mju.cs.timelapsesns.util.ImageUtils;
import kr.ac.mju.cs.timelapsesns.util.PermissionUtils;
import kr.ac.mju.cs.timelapsesns.util.SharedPreferencesUtil;

public class  MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.container) ViewPager viewPager;

    private DatabaseReference rootRef;
    private FirebaseAuth auth;

    private boolean isAlarmOn = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //TODO: onResume으로 옮기고 일부 권한은 다른 창으로 이동.
        // 실행에 필요한 권한 획득
        PermissionUtils.requestForPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION, BasicInfo.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        PermissionUtils.requestForPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION, BasicInfo.PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
        PermissionUtils.requestForPermission(this, android.Manifest.permission.CAMERA, BasicInfo.PERMISSIONS_REQUEST_CAMERA);
        PermissionUtils.requestForPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE, BasicInfo.PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        PermissionUtils.requestForPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, BasicInfo.PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

        // TimeZone을 Asia/Seoul로 설정
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Seoul"));
        JodaTimeAndroid.init(this);

        // Firebase 초기화
        auth = FirebaseAuth.getInstance();
        rootRef = FirebaseDatabase.getInstance().getReference();
        Log.i(TAG, "Connected Firebase DB: " + rootRef.toString());

        // 각 섹션의 fragment를 반환하는 어댑터 생성
        FragmentPagerAdapter pagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            private final Fragment[] mFragments = new Fragment[] {
                    new RecentPostsFragment(),
                    new MyPostsFragment(),
            };
            private final String[] mFragmentNames = new String[] {
                    getString(R.string.heading_recent),
                    getString(R.string.heading_my_posts),
            };
            @Override
            public Fragment getItem(int position) {
                return mFragments[position];
            }
            @Override
            public int getCount() {
                return mFragments.length;
            }
            @Override
            public CharSequence getPageTitle(int position) {
                return mFragmentNames[position];
            }
        };
        // viewPager를 어댑터에 설정
        viewPager = findViewById(R.id.container);
        viewPager.setAdapter(pagerAdapter);
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    @OnClick(R.id.fab_new_post)
    public void onFloatingButtonClicked() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state))
            Toast.makeText(this, "Error: External Storage not Mounted.", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.alarm_on).setTitle(Html.fromHtml("<font color='#FF4081'>알람켜짐</font>"));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                //익명 상태는 굳이 파이어베이스 아이디를 clear 해줄 필요 없다.
                if(AccessToken.getCurrentAccessToken() != null)
                    LoginManager.getInstance().logOut();

                SharedPreferencesUtil.getInstance().setAuthResult(BasicInfo.LOGOUT);

                startActivity(new Intent(this, SignInActivity.class));
                finish();
                break;
            case R.id.add_picture:
                if(SharedPreferencesUtil.getInstance().getAuthResultKey().equals(BasicInfo.ANONYMOUS)) {
                    Toast.makeText(this, "로그인이 필요한 서비스입니다.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this, FacebookLoginActivity.class);
                    startActivity(intent);
                }
                else
                    ImageUtils.fetchImage(MainActivity.this);
                break;
            case R.id.alarm_on:
                if(isAlarmOn == true) {
                    item.setTitle(Html.fromHtml("<font color='#000000'>알람꺼짐</font>"));
                    isAlarmOn = false;
                    SharedPreferencesUtil.getInstance().setAlarm(isAlarmOn);
                }
                else {
                    item.setTitle(Html.fromHtml("<font color='#FF4081'>알람켜짐</font>"));
                    isAlarmOn = true;
                    SharedPreferencesUtil.getInstance().setAlarm(isAlarmOn);
                }
                break;
//            case R.id.clear:
//                if(AccessToken.getCurrentAccessToken() != null)
//                    LoginManager.getInstance().logOut();
//                auth.signOut();
//
//                SharedPreferencesUtil.getInstance().setAuthResult(BasicInfo.LOGOUT);
//
//                startActivity(new Intent(this, SignInActivity.class));
//                finish();
//                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == BasicInfo.PICK_FROM_ALBUM
                || requestCode == BasicInfo.PICK_FROM_CAMERA) {
            if (resultCode != RESULT_OK) {
                switch (requestCode) {
                    case BasicInfo.PICK_FROM_ALBUM:
                        Toast.makeText(this, "앨범에서 사진을 가져오는 데 실패했습니다", Toast.LENGTH_SHORT).show();
                        break;
                    case BasicInfo.PICK_FROM_CAMERA:
                        Toast.makeText(this, "찍은 사진을 업로드하는 데 실패했습니다", Toast.LENGTH_SHORT).show();
                        break;
                }
                return;
            }

            Uri fileUri;
            if(requestCode == BasicInfo.PICK_FROM_CAMERA) {
                fileUri = ImageUtils.fileUri;
                if(fileUri == null)
                    return;
            }
            else {
                fileUri = data.getData();
                if (fileUri == null && ImageUtils.findImageFileNameFromUri(this, fileUri) == false)
                    return;
            }

            Log.d(TAG, "fileUri: " + fileUri.toString());   //fileUri: content://media/external/images/media/11296
            Intent mapIntent = new Intent(this, MapsActivity.class);
            mapIntent.putExtra(BasicInfo.EXTRA_FILE_URI, fileUri);
            startActivity(mapIntent);
        }
    }

}
