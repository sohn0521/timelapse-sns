package kr.ac.mju.cs.timelapsesns.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.TimelapseSnsApplication;
import kr.ac.mju.cs.timelapsesns.activity.login.FacebookLoginActivity;
import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;
import kr.ac.mju.cs.timelapsesns.fragment.dialog.EditorDialogFragment;
import kr.ac.mju.cs.timelapsesns.service.GpsTrackService;
import kr.ac.mju.cs.timelapsesns.util.SharedPreferencesUtil;

public class MapsActivity extends BaseActivity
        implements OnMapReadyCallback, GeoQueryEventListener,
        GoogleMap.OnCameraChangeListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    private static final String TAG = MapsActivity.class.getSimpleName();

    private GoogleMap mMap;
    private Circle searchCircle;
    private GeoFire geoFire;
    private GeoQuery geoQuery;

    private boolean listenerAttaching = false;
    private Uri fileUri = null;

    private Map<String, Marker> markers;
    private LatLng clickedLatLng;
    private Double myLat;
    private Double myLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Intent intent = getIntent();
        fileUri = intent.getParcelableExtra(BasicInfo.EXTRA_FILE_URI);

        geoFire = new GeoFire((FirebaseDatabase.getInstance(FirebaseApp.getInstance())
                .getReferenceFromUrl(BasicInfo.GEO_FIRE_REF)));

        showProgressDialog();
        Intent serviceIntent = new Intent(MapsActivity.this, GpsTrackService.class);
        startService(serviceIntent);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        hideProgressDialog();

        LatLng latLngCenter = new LatLng(myLat, myLng);
        if(latLngCenter.longitude == 0 && latLngCenter.latitude == 0)
            Toast.makeText(this, "내 위치를 가져올 수 없습니다.", Toast.LENGTH_SHORT).show();

        searchCircle = mMap.addCircle(new CircleOptions().center(latLngCenter).radius(1000));
        searchCircle.setFillColor(Color.argb(66, 200, 0, 255));
        searchCircle.setStrokeColor(Color.argb(66, 0, 0, 0));

        // Add a marker in Sydney and move the camera
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngCenter, BasicInfo.INITIAL_ZOOM_LEVEL));
        mMap.setOnCameraChangeListener(this);
        if(fileUri != null) mMap.setOnMapClickListener(this);
        else mMap.setOnMarkerClickListener(this);

        // setup GeoFire
        GeoLocation myLocation = new GeoLocation(latLngCenter.latitude, latLngCenter.longitude);
        geoQuery = geoFire.queryAtLocation(myLocation, 1);    //km
        geoQuery.addGeoQueryEventListener(this);
        listenerAttaching = true;

        // setup markers
        markers = new HashMap<String, Marker>();
    }

    private DialogInterface.OnClickListener yesListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if(SharedPreferencesUtil.getInstance().getAuthResultKey().equals(BasicInfo.ANONYMOUS)) {
                Toast.makeText(TimelapseSnsApplication.getContext(), "로그인이 필요한 서비스입니다.", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MapsActivity.this, FacebookLoginActivity.class);
                startActivity(intent);
            } else {
                EditorDialogFragment editorDialog = EditorDialogFragment.newInstance(fileUri, clickedLatLng.latitude, clickedLatLng.longitude);
                editorDialog.setOnAdapterItemClickListener(new EditorDialogFragment.OnAdapterItemClickLIstener() {
                    @Override
                    public void onAdapterItemClick(String finish) {
                        if(finish.equals("success")) finish();
                    }
                });
                editorDialog.show(getSupportFragmentManager(), "editor_dialog");
            }

            dialog.dismiss();
        }
    };
    private DialogInterface.OnClickListener noListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    };

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(BasicInfo.ACTION_MY_LOCATION)) {
                myLat = intent.getDoubleExtra(BasicInfo.EXTRA_LAT_LOCATION, 0);
                myLng = intent.getDoubleExtra(BasicInfo.EXTRA_LNG_LOCATION, 0);

                Log.d("qwer", myLat + ", " + myLng);
                setGoogleMap();
            }
            else {
                ActivityCompat.requestPermissions(MapsActivity.this, new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION}, BasicInfo.RC_PERMISSION);
            }
        }
    };

    private void setGoogleMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // add an event listener to start updating locations again
        if(listenerAttaching == true)
            geoQuery.addGeoQueryEventListener(this);

        IntentFilter filter = new IntentFilter();
        filter.addAction(BasicInfo.ACTION_MY_LOCATION);
        filter.addAction(BasicInfo.ACTION_REQUEST_PERMISSIONS);
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();

        hideProgressDialog();
        // remove all event listeners to stop updating in the background
        geoQuery.removeAllListeners();
        for (Marker marker: markers.values()) {
            marker.remove();
        }
        markers.clear();

        unregisterReceiver(mReceiver);

        Intent serviceIntent = new Intent(MapsActivity.this, GpsTrackService.class);
        stopService(serviceIntent);
    }

    @Override
    public void onKeyEntered(String key, GeoLocation location) {
        if(mMap != null && markers != null) {
            Marker marker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(location.latitude, location.longitude)));
            marker.setTag(new PostKeyTag(key));
            markers.put(key, marker);
        }
    }

    @Override
    public void onKeyExited(String key) {
        Marker marker = markers.get(key);
        if (marker != null) {
            marker.remove();
            markers.remove(key);
        }
    }

    @Override
    public void onKeyMoved(String key, GeoLocation location) {
        Marker marker = markers.get(key);
        if (marker != null)
            animateMarkerTo(marker, location.latitude, location.longitude);
    }

    @Override
    public void onGeoQueryReady() {

    }

    @Override
    public void onGeoQueryError(DatabaseError error) {

    }

    // Animation handler for old APIs without animation support
    private void animateMarkerTo(final Marker marker, final double lat, final double lng) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long DURATION_MS = 3000;
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final LatLng startPosition = marker.getPosition();
        handler.post(new Runnable() {
            @Override
            public void run() {
                float elapsed = SystemClock.uptimeMillis() - start;
                float t = elapsed/DURATION_MS;
                float v = interpolator.getInterpolation(t);

                double currentLat = (lat - startPosition.latitude) * v + startPosition.latitude;
                double currentLng = (lng - startPosition.longitude) * v + startPosition.longitude;
                marker.setPosition(new LatLng(currentLat, currentLng));

                // if animation is not finished yet, repeat
                if (t < 1) {
                    handler.postDelayed(this, 16);
                }
            }
        });
    }


    private double zoomLevelToRadius(double zoomLevel) {
        // Approximation to fit circle into view
        return 16384000/Math.pow(2, zoomLevel);
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        // Update the search criteria for this geoQuery and the circle on the ic_map
        LatLng center = cameraPosition.target;
        double radius = zoomLevelToRadius(cameraPosition.zoom);
        searchCircle.setCenter(center);
        searchCircle.setRadius(radius);
        geoQuery.setCenter(new GeoLocation(center.latitude, center.longitude));
        geoQuery.setRadius(radius/1000);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        clickedLatLng = latLng;
        AlertDialog dialog = new AlertDialog.Builder(MapsActivity.this)
                .setTitle("이 위치에 등록하시겠습니까?")
                .setPositiveButton("네", yesListener)
                .setNegativeButton("아니오", noListener)
                .create();
        dialog.show();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        PostKeyTag postKey = (PostKeyTag) marker.getTag();

        Intent intent = new Intent(this, TimelapseActivity.class);
        intent.putExtra(BasicInfo.EXTRA_POST_KEY, postKey.toString());
        startActivity(intent);

        return false;
    }

    private static class PostKeyTag {
        private String postKey;

        public PostKeyTag(String postKey) {
            this.postKey = postKey;
        }

        @Override
        public String toString() {
            return postKey;
        }
    }
}
