package kr.ac.mju.cs.timelapsesns.viewholder;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.model.Post;
import kr.ac.mju.cs.timelapsesns.util.PostUtils;
import kr.ac.mju.cs.timelapsesns.util.TimeUtils;

import static kr.ac.mju.cs.timelapsesns.TimelapseSnsApplication.getContext;

public class PostViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = PostViewHolder.class.getName();

    @BindView(R.id.post_photo)
    public ImageView postPhoto;

    @BindView(R.id.post_title)
    public TextView titleView;

    @BindView(R.id.post_author)
    public TextView authorView;

    @BindView(R.id.post_follow)
    public ImageView followView;

    @BindView(R.id.post_like)
    public ImageView likeView;

    @BindView(R.id.post_num_likes)
    public TextView numLikesView;

    @BindView(R.id.post_posted_at)
    public TextView postedAtView;

    @BindView(R.id.post_body)
    public TextView bodyView;

    @BindView(R.id.post_privacy)
    public TextView privacyView;

    public PostViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }


    public void bind(Post post,
                     View.OnClickListener followClickListener,
                     View.OnClickListener likeClickListener) {
        Log.d(TAG, "bind to post");
        Glide.with(getContext())
                .load(PostUtils.getThumbnailUri(post))
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(postPhoto);
        titleView.setText(post.title);
        authorView.setText(post.author);
        numLikesView.setText(String.valueOf(post.likeCount));
        bodyView.setText(post.body);

        if (post.postedAt != null) {
            postedAtView.setText(TimeUtils.convertEpochIntoStr(post.postedAt));
        }

        String privacy = "";
        if (post.privacy != null && post.privacy.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String str : post.privacy) {
                sb.append(str);
                sb.append(", ");
            }
            privacy = sb.substring(0, sb.length() - 2);
        }
        privacyView.setText(privacy);

        followView.setOnClickListener(followClickListener);
        likeView.setOnClickListener(likeClickListener);
    }
}
