package kr.ac.mju.cs.timelapsesns.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;

@SuppressWarnings("ConstantConditions")
public class ImageUtils {
    public static Uri fileUri;

    /**
     * 업로드할 이미지를 가져올 방법을 카메라와 앨범 중 선택한다
     */
    public static void fetchImage(final Activity context) {
        AlertDialog dialog;
        DialogInterface.OnClickListener cameraListener = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                // 카메라 액션 실행
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String fileName = "upload_" + String.valueOf(System.currentTimeMillis() / 1000) + ".jpg";
                File myImageDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName);
                fileUri = FileProvider.getUriForFile(context,
                        context.getPackageName() + ".provider", myImageDir);
                cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, fileUri);
                if (cameraIntent.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivityForResult(cameraIntent, BasicInfo.PICK_FROM_CAMERA);
                }
            }
        };

        DialogInterface.OnClickListener albumListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                // 앨범 선택액션 실행
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
                context.startActivityForResult(intent, BasicInfo.PICK_FROM_ALBUM);
            }
        };

        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };

        dialog = new AlertDialog.Builder(context)
                .setTitle("업로드할 이미지 선택")
                .setPositiveButton("앨범선택", albumListener)
                .setNegativeButton("사진촬영", cameraListener)
                .setNeutralButton("취소", cancelListener).create();

        dialog.show();
    }

    public static boolean findImageFileNameFromUri(Activity context, Uri tempUri) {
        boolean success = false;

        // 실제 Image Uri의 절대이름
        String[] IMAGE_DB_COLUMN = {MediaStore.Images.ImageColumns.DATA};
        Cursor cursor = null;
        try {
            // Primary Key값을 추출
            String imagePK = String.valueOf(ContentUris.parseId(tempUri));
            // Image DB에 쿼리를 날린다.
            cursor = context.getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    IMAGE_DB_COLUMN,
                    MediaStore.Images.Media._ID + "=?",
                    new String[]{imagePK}, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA));
                success = true;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return success;
    }

    /**
     * Bitmap 이미지 파일을 임시파일로 저장한다
     *
     * @param tempBitmap 저장할 이미지 파일
     * @param imageDir   카메라로 찍은 사진을 저장할 디렉토리
     * @return 성공 여부
     */
    private boolean tempSavedBitmapFile(Bitmap tempBitmap, File imageDir) throws IOException {
        boolean success = false;

        // 임시파일 생성
        String fileName = "upload_" + (System.currentTimeMillis() / 1000);
        String fileSuffix = ".jpg";
        File tempFile = File.createTempFile(fileName, fileSuffix, imageDir);

        // 이미지를 임시파일에 저장
        try (FileOutputStream bitmapStream = new FileOutputStream(tempFile)) {
            tempBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bitmapStream);
            Uri fileUri = Uri.fromFile(tempFile);
            success = true;
        }
        return success;
    }
}
