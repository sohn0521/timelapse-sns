package kr.ac.mju.cs.timelapsesns.rest;

import com.google.gson.JsonObject;

import io.reactivex.Observable;
import retrofit2.http.POST;

/**
 * Created by Tak on 2018. 1. 1..
 */

public interface FcmService {
    @POST
    Observable<JsonObject> sendFcmMessage(String body, String receiver);
}
