package kr.ac.mju.cs.timelapsesns.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;
import kr.ac.mju.cs.timelapsesns.model.ParcelableUser;
import kr.ac.mju.cs.timelapsesns.viewholder.UserSelectionViewHolder;
import kr.ac.mju.cs.timelapsesns.viewholder.UserViewHolder;

/**
 * Created by Tak on 2017. 12. 29..
 */

public class UserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements UserViewHolder.OnUserItemClickListener, UserSelectionViewHolder.OnUserItemClickListener {

    private ArrayList<String> selectedUserList;
    private ArrayList<ParcelableUser> userList; //view_type_user
    private ArrayList<String> userNameList;     //view_type_user_selection
    private Context mContext;
    private OnUserItemClickListener listener;
    private SparseBooleanArray itemSelected;
    private int mViewType;

    public UserAdapter(Context context, int viewType) {
        mContext = context;
        mViewType = viewType;

        if(viewType == BasicInfo.VIEW_TYPE_USER_SELECTION)
            userNameList = new ArrayList<>();
        else if(viewType == BasicInfo.VIEW_TYPE_USER) {
            selectedUserList = new ArrayList<>();
            itemSelected = new SparseBooleanArray();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        viewType = mViewType;

        if(viewType == BasicInfo.VIEW_TYPE_USER) {
            UserViewHolder viewHolder
                    = new UserViewHolder(inflater.inflate(R.layout.item_user, viewGroup, false));
            viewHolder.setOnUserItemClickListener(this);
            return viewHolder;
        } else if(viewType == BasicInfo.VIEW_TYPE_USER_SELECTION) {
            UserSelectionViewHolder viewHolder
                    = new UserSelectionViewHolder(inflater.inflate(R.layout.item_user_selection, viewGroup, false));
            viewHolder.setOnUserItemClickListener(this);
            return viewHolder;
        }

        throw new IllegalArgumentException("invalid viewtype");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(mViewType == BasicInfo.VIEW_TYPE_USER) {
            //TODO: 캐스팅 다시보기
            UserViewHolder userViewHolder = (UserViewHolder) holder;
            userViewHolder.bind(userList.get(position));
            userViewHolder.setChecked(itemSelected.get(position));
        } else if(mViewType == BasicInfo.VIEW_TYPE_USER_SELECTION) {
            UserSelectionViewHolder userSelectionViewHolder = (UserSelectionViewHolder) holder;
            userSelectionViewHolder.bind(userNameList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if(mViewType == BasicInfo.VIEW_TYPE_USER)
            return userList == null ? 0 : userList.size();
        else
            return userNameList == null ? 0 : userNameList.size();
    }

    public void addUser(ArrayList<ParcelableUser> searchUserList) {
        userList = searchUserList;
        notifyDataSetChanged();
    }

    public void addUserName(String username) {
        userNameList.add(username);
        notifyDataSetChanged();
    }

    //탐색 결과로 나온 유저에 대한 클릭
    @Override
    public void onUserClicked(View view, ParcelableUser user, int position) {
        boolean checked = itemSelected.get(position);
        setItemChecked(position, !checked);

        boolean isSelected = false;
        for(int i=0; i<selectedUserList.size(); i++)
            if(selectedUserList.get(i).equals(user.username))
                isSelected = true;

        if(listener != null && isSelected == false) {
            selectedUserList.add(user.username);
            listener.onUserClicked(view, user, position);
        }
    }

    //선택된 유저 중 지우는 목적의 클릭
    @Override
    public void onUserNameClicked(View view, String username, int position) {
        userNameList.remove(position);
        notifyDataSetChanged();

        if(listener != null)
            listener.onUserNameClicked(view, username, position);
    }

    private void setItemChecked(int position, boolean isChecked) {
        boolean checked = itemSelected.get(position);
        if (checked != isChecked) {
            itemSelected.put(position, isChecked);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        itemSelected.clear();
    }

    public interface OnUserItemClickListener {
        public void onUserClicked(View view, ParcelableUser user, int position);
        public void onUserNameClicked(View view, String username, int position);
    }

    public void setOnUserItemClickListener(OnUserItemClickListener listener) {
        this.listener = listener;
    }
}
