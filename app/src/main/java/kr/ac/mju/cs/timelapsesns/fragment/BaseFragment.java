package kr.ac.mju.cs.timelapsesns.fragment;


import android.app.ProgressDialog;
import android.support.v4.app.DialogFragment;

/**
 * Created by Tak on 2018. 1. 8..
 */

public class BaseFragment extends DialogFragment {
    private ProgressDialog progressDialog;

    public void showProgressDialog() {
        showProgressDialog("로딩중...");
    }

    public void showProgressDialog(String caption) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setCancelable(false);
        }

        progressDialog.setMessage(caption);
        progressDialog.show();
    }


    public void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
