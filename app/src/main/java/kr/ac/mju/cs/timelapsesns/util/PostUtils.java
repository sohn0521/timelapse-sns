package kr.ac.mju.cs.timelapsesns.util;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import kr.ac.mju.cs.timelapsesns.model.Post;

public class PostUtils {

    public static String getNthImageUri(Post post, int n) {
        return post.photoGroupUri + post.filenames.get(n) + "?alt=media";
    }

    public static String getThumbnailUri(Post post) {
        return getNthImageUri(post, 0);
    }

    public static List<String> getImageUris(Post post) {
        List<String> uris = new ArrayList<>();
        int size = post.filenames.size();
        for (int i = 0; i < size; i++) {
            uris.add(getNthImageUri(post, i));
        }

        return uris;
    }
}
