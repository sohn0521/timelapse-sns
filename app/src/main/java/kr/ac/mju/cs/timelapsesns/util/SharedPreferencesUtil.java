package kr.ac.mju.cs.timelapsesns.util;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import kr.ac.mju.cs.timelapsesns.TimelapseSnsApplication;

/**
 * Created by Tak on 2017. 12. 22..
 */

public class SharedPreferencesUtil {
    private static SharedPreferencesUtil instance;

    public static SharedPreferencesUtil getInstance() {
        if(instance == null)
            instance = new SharedPreferencesUtil();

        return instance;
    }

    private static final String FCM_TOKEN_KEY = "fcm_token_key";
    private static final String AUTH_RESULT_KEY = "auth_result_key";
    private static final String FIREBASE_UID = "firebase_uid";
    private static final String ALARM_ON = "alarm_on";

    private static final String TITLE = "title";
    private static final String BODY = "body";
    private static final String USERS = "users";
    private static final String DATE = "date";
    private static final String PRIVACY = "privacy";

    SharedPreferences mPref;
    SharedPreferences.Editor mEditor;

    private SharedPreferencesUtil() {
        mPref = PreferenceManager.getDefaultSharedPreferences(TimelapseSnsApplication.getContext());
        mEditor = mPref.edit();
    }

    public void clear() {
        mEditor.clear();
        mEditor.commit();
    }

    public void clearPostContent() {
        mEditor.remove(TITLE).commit();
        mEditor.remove(BODY).commit();
        mEditor.remove(DATE).commit();
    }

    public void setFirebaseUid(String uid) {
        mEditor.putString(FIREBASE_UID, uid);
        mEditor.commit();
    }

    public String getFirebaseUid() {
        return mPref.getString(FIREBASE_UID, "");
    }

    public void setAuthResult(String provider) {
        mEditor.putString(AUTH_RESULT_KEY, provider);
        mEditor.commit();
    }

    public String getAuthResultKey() {
        return mPref.getString(AUTH_RESULT_KEY, "");
    }

    public void setFcmTokenKey(String fcmTokenKey){
        mEditor.putString(FCM_TOKEN_KEY, fcmTokenKey);
        mEditor.commit();
    }
    public  String getFcmTokenKey() {
        return mPref.getString(FCM_TOKEN_KEY,"");
    }

    public void setAlarm(boolean alarmOn) {
        mEditor.putBoolean(ALARM_ON, alarmOn);
        mEditor.commit();
    }

    public boolean getAlarm() {
        return mPref.getBoolean(ALARM_ON, true);
    }

    public void setTitle(String title) {
        mEditor.putString(TITLE, title);
        mEditor.commit();
    }
    public String getTitle() {
        return mPref.getString(TITLE, "");
    }

    public void setBody(String body) {
        mEditor.putString(BODY, body);
        mEditor.commit();
    }
    public String getBody() {
        return mPref.getString(BODY, "");
    }

    public void setDate(String date) {
        mEditor.putString(DATE, date);
        mEditor.commit();
    }

    public String getDate() {
        return mPref.getString(DATE, "");
    }
}
