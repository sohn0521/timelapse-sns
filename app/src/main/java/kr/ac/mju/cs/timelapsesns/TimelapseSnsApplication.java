package kr.ac.mju.cs.timelapsesns;

import android.app.Application;
import android.content.Context;

import com.firebase.client.Firebase;

/**
 * Created by Tak on 2017. 11. 22..
 */

public class TimelapseSnsApplication extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            // This process is dedicated to LeakCanary for heap analysis.
//            // You should not init your app in this process.
//            return;
//        }
//        LeakCanary.install(this);
        Firebase.setAndroidContext(this);
//        KakaoSDK.init(new KakaoSDKAdapter());
    }

    public static Context getContext() {
        return context;
    }

//    private static class KakaoSDKAdapter extends KakaoAdapter {
//        /**
//         * Session Config에 대해서는 default값들이 존재한다.
//         * 필요한 상황에서만 override해서 사용하면 됨.
//         * @return Session의 설정값.
//         */
//        @Override
//        public ISessionConfig getSessionConfig() {
//            return new ISessionConfig() {
//                @Override
//                public AuthType[] getAuthTypes() {
//                    return new AuthType[] {AuthType.KAKAO_LOGIN_ALL};
//                }
//
//                @Override
//                public boolean isUsingWebviewTimer() {
//                    return false;
//                }
//
//                @Override
//                public ApprovalType getApprovalType() {
//                    return ApprovalType.INDIVIDUAL;
//                }
//
//                @Override
//                public boolean isSaveFormData() {
//                    return true;
//                }
//            };
//        }
//
//        @Override
//        public IApplicationConfig getApplicationConfig() {
//            return new IApplicationConfig() {
//                @Override
//                public Activity getTopActivity() {
//                    return null;
//                }
//
//                @Override
//                public Context getApplicationContext() {
//                    return TimelapseSnsApplication.getContext();
//                }
//            };
//        }
//    }
}
