package kr.ac.mju.cs.timelapsesns.util;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {

    public static MatchResult findNthOccurance(int n, Pattern p, CharSequence src){
        Matcher m = p.matcher(src);
        for(int i = 0; i<n; i++) m.find();
        return m;
    }
}
