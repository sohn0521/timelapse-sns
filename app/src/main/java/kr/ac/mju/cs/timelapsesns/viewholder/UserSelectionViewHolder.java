package kr.ac.mju.cs.timelapsesns.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.ac.mju.cs.timelapsesns.R;

/**
 * Created by Tak on 2017. 12. 30..
 */

public class UserSelectionViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.user_name)
    TextView userName;

    public UserSelectionViewHolder(final View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null)
                    listener.onUserNameClicked(view, username, getAdapterPosition());
            }
        });
    }

    private String username;
    public void bind(String name) {
        username = name;
        userName.setText(name);
    }

    public interface OnUserItemClickListener {
        public void onUserNameClicked(View view, String username, int position);
    }

    OnUserItemClickListener listener;
    public void setOnUserItemClickListener(OnUserItemClickListener listener) {
        this.listener = listener;
    }
}
