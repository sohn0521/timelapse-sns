package kr.ac.mju.cs.timelapsesns.util;

import android.net.Uri;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringParser {

    public static String parseLocalDir(Uri fileUri) {
        return fileUri.toString().substring(0, fileUri.toString().lastIndexOf("/"));
    }

    public static String parsePhotoGroupUri(Uri downloadUri) {
        MatchResult lastSlash = getLastSlash(downloadUri);
        return downloadUri.toString().substring(0, lastSlash.end());
    }

    public static String parsePhotoFilename(Uri downloadUri) {
        MatchResult lastSlash = getLastSlash(downloadUri);
        Matcher m = Pattern.compile("\\?").matcher(downloadUri.toString());
        if (!m.find()) return null;
        List<String> filenames = new ArrayList<>();
        return downloadUri.toString().substring(lastSlash.end(), m.start());
    }

    private static MatchResult getLastSlash(Uri downloadUri) {
        return RegexUtils.findNthOccurance(3, Pattern.compile("%2F"), downloadUri.toString());
    }
}
