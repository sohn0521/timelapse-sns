package kr.ac.mju.cs.timelapsesns.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Tak on 2018. 1. 8..
 */

public class ParcelableUser implements Parcelable {
    public String username;
    public String uid;
    public String fcmToken;
    public boolean isSelected;

    public ParcelableUser(String username, String uid, String fcmToken, boolean isSelected) {
        this.username = username;
        this.uid = uid;
        this.fcmToken = fcmToken;
        this.isSelected = isSelected;
    }

    protected ParcelableUser(Parcel in) {
        username = in.readString();
        uid = in.readString();
        fcmToken = in.readString();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<ParcelableUser> CREATOR = new Creator<ParcelableUser>() {
        @Override
        public ParcelableUser createFromParcel(Parcel in) {
            return new ParcelableUser(in);
        }

        @Override
        public ParcelableUser[] newArray(int size) {
            return new ParcelableUser[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(uid);
        parcel.writeString(fcmToken);
        parcel.writeByte((byte) (isSelected ? 1 : 0));
    }
}
