package kr.ac.mju.cs.timelapsesns.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("UnusedDeclaration")
public class User {

    public String uid;
    public String username;
    public String email;
    public String fcmToken;
    public int followingCount = 0;
    public HashMap<String, Boolean> followings = new HashMap<>();
    public int followerCount = 0;
    public HashMap<String, Boolean> followers = new HashMap<>();

    public User() {}

    public User(String uid) {
        this(uid, null, null, null);
    }

    public User(String uid, String username, String email, String fcmToken) {
        this.uid = uid;
        this.username = username;
        this.email = email;
        this.fcmToken = fcmToken;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("username", username);
        result.put("email", email);
        result.put("fcmToken", fcmToken);
        result.put("followingCount", followingCount);
        result.put("followings", followings);
        result.put("followerCount", followerCount);
        result.put("followers", followers);

        return result;
    }
}
