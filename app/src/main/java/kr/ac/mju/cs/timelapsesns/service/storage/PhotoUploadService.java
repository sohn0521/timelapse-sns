package kr.ac.mju.cs.timelapsesns.service.storage;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import kr.ac.mju.cs.timelapsesns.activity.MainActivity;
import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.TimelapseSnsApplication;
import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;

@SuppressWarnings("ConstantConditions")
public class PhotoUploadService extends BaseTaskService {
    private static final String TAG = PhotoUploadService.class.getName();

    private String actionType;

    private StorageReference storageRef;

    public PhotoUploadService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        storageRef = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        actionType = intent.getAction();
        if (BasicInfo.ACTION_NEW_UPLOAD.equals(intent.getAction()) ||
                BasicInfo.ACTION_APPEND_UPLOAD.equals(intent.getAction())) {
            String uid = intent.getStringExtra(BasicInfo.EXTRA_UID);
            String postKey = intent.getStringExtra(BasicInfo.EXTRA_POST_KEY);
            Uri fileUri = intent.getParcelableExtra(BasicInfo.EXTRA_FILE_URI);
            uploadFromUri(uid, postKey, fileUri);
        }

        return START_REDELIVER_INTENT;
    }

    private void uploadFromUri(@NonNull String uid, @NonNull String postKey, @NonNull final Uri fileUri) {
        Log.d(TAG, "uploadFromUri: " + fileUri.toString());

        taskStarted();
        showProgressNotification(getString(R.string.progress_downloading), 0, 0);

        Log.d(TAG, "fileUri.getLastPathSegment(): " + fileUri.getLastPathSegment());
        StorageReference photoRef = storageRef
                .child("photo_group")
                .child(uid)
                .child(postKey)
                .child(fileUri.getLastPathSegment());

        Log.d(TAG, "uploadFromUri:dst:" + photoRef.getPath());
        photoRef.putFile(fileUri).
                addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        showProgressNotification(getString(R.string.progress_uploading),
                                taskSnapshot.getBytesTransferred(),
                                taskSnapshot.getTotalByteCount());
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Uri downloadUri = taskSnapshot.getMetadata().getDownloadUrl();
                        Log.d(TAG, "onSuccess " + downloadUri.toString());
                        broadcastUploadFinished(downloadUri, fileUri);
                        showUploadFinishedNotification(downloadUri, fileUri);
                        taskCompleted();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure", e);

                        broadcastUploadFinished(null, fileUri);
                        showUploadFinishedNotification(null, fileUri);
                        taskCompleted();
                    }
                });
    }

    private void broadcastUploadFinished(@Nullable Uri downloadUri, @Nullable Uri fileUri) {
        Log.d(TAG, "Finished upload: " + downloadUri.toString());
        boolean success = downloadUri != null;

        String action = "";
        if (BasicInfo.ACTION_NEW_UPLOAD.equals(actionType)) {
            action = success ? BasicInfo.NEW_UPLOAD_COMPLETED : BasicInfo.NEW_UPLOAD_ERROR;
        } else if (BasicInfo.ACTION_APPEND_UPLOAD.equals(actionType)) {
            action = success ? BasicInfo.APPEND_UPLOAD_COMPLETED : BasicInfo.APPEND_UPLOAD_ERROR;
        } else {
            return;
        }

        Intent broadcast = new Intent(action)
                .putExtra(BasicInfo.EXTRA_DOWNLOAD_URL, downloadUri)
                .putExtra(BasicInfo.EXTRA_FILE_URI, fileUri);

        LocalBroadcastManager.getInstance(TimelapseSnsApplication.getContext())
                .sendBroadcast(broadcast);
    }

    private void showUploadFinishedNotification(@Nullable Uri downloadUrl, @Nullable Uri fileUri) {
        dismissProgressNotification();

        Intent intent = new Intent(this, MainActivity.class)
                .putExtra(BasicInfo.EXTRA_DOWNLOAD_URL, downloadUrl)
                .putExtra(BasicInfo.EXTRA_FILE_URI, fileUri)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        boolean success = downloadUrl != null;

        String caption = success ? getString(R.string.upload_success) : getString(R.string.upload_failure);
        showFinishedNotification(caption, intent, success);
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BasicInfo.NEW_UPLOAD_COMPLETED);
        filter.addAction(BasicInfo.NEW_UPLOAD_ERROR);
        filter.addAction(BasicInfo.APPEND_UPLOAD_COMPLETED);
        filter.addAction(BasicInfo.APPEND_UPLOAD_ERROR);

        return filter;
    }
}
