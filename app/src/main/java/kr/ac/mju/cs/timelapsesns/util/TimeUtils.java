package kr.ac.mju.cs.timelapsesns.util;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Created by sohn0521 on 07/12/2017.
 */

public class TimeUtils {

    public static final String STANDARD_FORMAT = "yy-MM-dd HH:mm:ss";

    public static DateTime getNow() {
        return new DateTime();
    }

    public static long getNowInEpoch() {
        return new DateTime().getMillis();
    }

    public static DateTime convertEpochIntoDateTime(long epoch) {
        return new DateTime(epoch);
    }

    public static String convertEpochIntoStr(long epoch) {
        return DateTimeFormat.forPattern(STANDARD_FORMAT).print(epoch);
    }

    public static String format(DateTime dt) {
        return DateTimeFormat.forPattern(STANDARD_FORMAT).print(dt);
    }
}
