package kr.ac.mju.cs.timelapsesns.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;


public class BaseActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    /**
     * 기본 메시지로 진행 대화창을 보여준다
     */
    public void showProgressDialog() {
        showProgressDialog("로딩중...");
    }

    /**
     * 진행 대화창을 보여준다
     *
     * @param caption 대화창 내용
     */
    public void showProgressDialog(String caption) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
        }

        progressDialog.setMessage(caption);
        progressDialog.show();
    }

    /**
     * 진행 대화창을 숨긴다
     */
    public void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
}
