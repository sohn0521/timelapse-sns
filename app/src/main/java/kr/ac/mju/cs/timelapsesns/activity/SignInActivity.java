package kr.ac.mju.cs.timelapsesns.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;
import kr.ac.mju.cs.timelapsesns.model.User;
import kr.ac.mju.cs.timelapsesns.util.SharedPreferencesUtil;

public class SignInActivity extends BaseActivity {

    private static final String TAG = SignInActivity.class.getSimpleName();

    private DatabaseReference rootRef;
    private FirebaseAuth auth;

    @BindView(R.id.field_email)
    EditText etEmail;
    @BindView(R.id.field_password)
    EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        rootRef = FirebaseDatabase.getInstance().getReference();
        auth = FirebaseAuth.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();

        String authResult = SharedPreferencesUtil.getInstance().getAuthResultKey();
        Log.d("authResult", authResult);

        switch (authResult) {
            case BasicInfo.ANONYMOUS:
            case BasicInfo.FACEBOOK:
                startMainActivity();
                break;
            case "":
                signInAnonymous();
                break;
        }
    }

    @OnClick({R.id.button_sign_in, R.id.button_sign_up, R.id.btn_anonymous_sign_in})
    public void onSignClicked(View view) {
        switch (view.getId()) {
            case R.id.button_sign_in:
                Toast.makeText(this, "추후구현", Toast.LENGTH_SHORT).show();
//                signIn();
                break;
            case R.id.button_sign_up:
                Toast.makeText(this, "추후구현", Toast.LENGTH_SHORT).show();
//                signUp();
                break;
            case R.id.btn_anonymous_sign_in:
                if (FirebaseAuth.getInstance().getCurrentUser() == null)
                    signInAnonymous();
                else {
                    setAuthInfo(getUid(), BasicInfo.ANONYMOUS);
                    startMainActivity();
                }
                break;
        }
    }

    private void signIn() {
        Log.d(TAG, "signIn");
        if (!validateForm()) {
            return;
        }

        showProgressDialog();
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signIn:onComplete:" + task.isSuccessful());
                        hideProgressDialog();

                        if (task.isSuccessful()) {
                            setAuthInfo(task.getResult().getUser().getUid(), BasicInfo.EMAIL);
                        } else {
                            Toast.makeText(SignInActivity.this, "Sign In Failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void signUp() {
        Log.d(TAG, "signUp");
        if (!validateForm()) {
            return;
        }

        showProgressDialog();
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUser:onComplete:" + task.isSuccessful());
                        hideProgressDialog();

                        if (task.isSuccessful()) {
                            setAuthInfo(task.getResult().getUser().getUid(), BasicInfo.EMAIL);
                        } else {
                            Toast.makeText(SignInActivity.this, "Sign Up Failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void signInAnonymous() {
        onNewIntent(getIntent());
        showProgressDialog(getString(R.string.progress_auth));
        auth.signInAnonymously()
                .addOnSuccessListener(this, new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {

                        // 로그인에 성공하면 익명사용자 정보를 서버에 저장
                        String uid = getUid();
                        Log.d(TAG, "uid: " + uid);
                        User user = new User(uid);
                        rootRef.child("users").child(uid).setValue(user);

                        hideProgressDialog();
                        setAuthInfo(user.uid, BasicInfo.ANONYMOUS);

                        startMainActivity();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "auth fail", e);
                        hideProgressDialog();
                    }
                });
    }

    private void setAuthInfo(String uid, String authResult) {
        Log.d("uuuiddd", uid);
        // fcm token 생성 및 저장
        String fcmToken = FirebaseInstanceId.getInstance().getToken();
        rootRef.child("users").child(uid).child("fcmToken").setValue(fcmToken);
        SharedPreferencesUtil.getInstance().setFcmTokenKey(fcmToken);

        // firebase uid 값 저장
        SharedPreferencesUtil.getInstance().setFirebaseUid(uid);

        // 로그인 구별
        SharedPreferencesUtil.getInstance().setAuthResult(authResult);
    }

    private void startMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private String usernameFromEmail(String uid, String email) {

        if (TextUtils.isEmpty(email)) {
            return uid;
        }

        if (email.contains("@")) {
            return email.split("@")[0];
        } else {
            return email;
        }
    }

    private boolean validateForm() {
        boolean result = true;
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            etEmail.setError("Required");
            result = false;
        } else {
            etEmail.setError(null);
        }

        if (TextUtils.isEmpty(etPassword.getText().toString())) {
            etPassword.setError("Required");
            result = false;
        } else {
            etPassword.setError(null);
        }

        return result;
    }
}
