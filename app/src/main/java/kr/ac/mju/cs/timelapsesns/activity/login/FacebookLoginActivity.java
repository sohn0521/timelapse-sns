package kr.ac.mju.cs.timelapsesns.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.activity.BaseActivity;
import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;
import kr.ac.mju.cs.timelapsesns.util.SharedPreferencesUtil;

public class FacebookLoginActivity extends BaseActivity {

    private static final String TAG = FacebookLoginActivity.class.getSimpleName();

    @BindView(R.id.facebook_login_btn)
    LoginButton fbLoginBtn;

    private FirebaseAuth mAuth;
    private DatabaseReference rootDBReference;

    private CallbackManager mCallbackManager;
    private ProfileTracker profileTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_login);
        ButterKnife.bind(this);
        Log.d("zzcbbb", "123");

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        mAuth = FirebaseAuth.getInstance();
        rootDBReference = FirebaseDatabase.getInstance().getReference();

        mCallbackManager = CallbackManager.Factory.create();
        fbLoginBtn.setReadPermissions("email", "public_profile");
        fbLoginBtn.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                if(currentProfile != null) {
                    Log.d("current_profile_name", currentProfile.getName());

                    //유저 이름 추가
                    if(currentProfile != null)
                        updateUserWithName(currentProfile.getName());
                }
            }
        };
    }

    /**
     * firebase database에 유저의 이름을 추가.
     *
     * @param name current facebook user name
     */
    private void updateUserWithName(String name) {
        Log.d("addUser", "success?");
        rootDBReference.child("users").child(getUid()).child("username").setValue(name);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(profileTracker != null)
            profileTracker.stopTracking();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleFacebookAccessToken(final AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token.getToken());
        showProgressDialog();

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.getCurrentUser().linkWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = task.getResult().getUser();
                            Log.d(TAG, "linkWithCredential:success " + user.getDisplayName());

                            SharedPreferencesUtil.getInstance().setAuthResult(BasicInfo.FACEBOOK);
                            hideProgressDialog();
                            finish();
                        }
                        else {
                            Log.d(TAG, "linkWithCredential:failure", task.getException());
                            if(task.getException().toString().contains("already")) {
                                SharedPreferencesUtil.getInstance().setAuthResult(BasicInfo.FACEBOOK);
                                hideProgressDialog();
                                finish();
                            }
                        }
                    }
                });
    }
}
