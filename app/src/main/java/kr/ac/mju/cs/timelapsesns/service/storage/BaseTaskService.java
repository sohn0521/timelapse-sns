package kr.ac.mju.cs.timelapsesns.service.storage;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;

@SuppressWarnings("ConstantConditions")
public abstract class BaseTaskService extends Service {
    private static final String TAG = BaseTaskService.class.getName();

    private int mNumTasks = 0;

    public void taskStarted() {
        changeNumberOfTasks(1);
    }
    public void taskCompleted() {
        changeNumberOfTasks(-1);
    }

    private synchronized void changeNumberOfTasks(int delta) {
        Log.d(TAG, "changeNumOfTasks" + mNumTasks + ":" + delta);
        mNumTasks += delta;

        if(mNumTasks <= 0) {
            Log.d(TAG, "stopping");
            stopSelf();
        }
    }

    protected void showProgressNotification(String caption, long completedUnits, long totalUnits) {
        int percentComplete = 0;
        if (totalUnits > 0) {
            percentComplete = (int) (100 * completedUnits / totalUnits);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.file_upload)
                .setContentText(getString(R.string.app_name))
                .setContentText(caption)
                .setProgress(100, percentComplete, false)
                .setOngoing(true)
                .setAutoCancel(false);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(BasicInfo.FINISHED_NOTIFICATION_ID, builder.build());
    }

    protected void showFinishedNotification(String caption, Intent intent, boolean success) {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        int icon = success ? R.drawable.check_white : R.drawable.check_error;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(icon)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(caption)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager manager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        manager.notify(BasicInfo.FINISHED_NOTIFICATION_ID, builder.build());
    }

    protected void dismissProgressNotification() {
        NotificationManager manager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        manager.cancel(BasicInfo.PROGRESS_NOTIFICATION_ID);
    }
}
