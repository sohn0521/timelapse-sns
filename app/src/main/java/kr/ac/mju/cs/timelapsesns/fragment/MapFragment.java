package kr.ac.mju.cs.timelapsesns.fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.activity.TimelapseActivity;
import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;
import kr.ac.mju.cs.timelapsesns.model.Post;
import kr.ac.mju.cs.timelapsesns.model.User;
import kr.ac.mju.cs.timelapsesns.service.GpsTrackService;
import kr.ac.mju.cs.timelapsesns.util.FirebaseUtils;

@SuppressWarnings("ConstantConditions")
public class MapFragment extends Fragment
        implements OnMapReadyCallback, GeoQueryEventListener, GoogleMap.OnCameraMoveListener, GoogleMap.OnInfoWindowClickListener {

    public static final String EXTRA_LAT_LOCATION = "EXTRA_LAT_LOCATION";
    public static final String EXTRA_LNG_LOCATION = "EXTRA_LNG_LOCATION";

    private static final String TAG = MapFragment.class.getSimpleName();

    private static final String GEO_FIRE_DB = "https://timelapsesns.firebaseio.com";
    private static final String GEO_FIRE_REF = GEO_FIRE_DB + "/_geofire";

    private static final int RC_PERMISSION = 1;
    private static final int INITIAL_ZOOM_LEVEL = 14;

    protected MapView mapView;

    protected GoogleMap map;
    protected Circle searchCircle;
    protected GeoFire geoFire;
    protected GeoQuery geoQuery;

    private DatabaseReference rootRef;

    protected BroadcastReceiver gpsReceiver;

    private Map<String, String> markerInfos;
    protected Map<String, Marker> markers;
    protected LatLng clickedLatLng;
    protected Double myLat;
    protected Double myLng;

    public MapFragment() {
        // required
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        mapView = rootView.findViewById(R.id.map);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (mapView != null) {
            mapView.onCreate(savedInstanceState);
        }

        geoFire = new GeoFire((FirebaseDatabase.getInstance(FirebaseApp.getInstance())
                .getReferenceFromUrl(GEO_FIRE_REF)));
        rootRef = FirebaseDatabase.getInstance().getReference();

        markers = new HashMap<>();
        markerInfos = new HashMap<>();

        gpsReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(BasicInfo.ACTION_MY_LOCATION)) {
                    myLat = intent.getDoubleExtra(EXTRA_LAT_LOCATION, 0);
                    myLng = intent.getDoubleExtra(EXTRA_LNG_LOCATION, 0);

                    Log.d(TAG, myLat + ", " + myLng);
                    mapView.getMapAsync(MapFragment.this);
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION}, RC_PERMISSION);
                }
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();

        // Remove all event listeners to stop updating in the background
        geoQuery.removeAllListeners();
        for (Marker marker : markers.values()) {
            marker.remove();
        }
        markers.clear();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();

        Intent gpsService = new Intent(getContext(), GpsTrackService.class);
        getContext().startService(gpsService);

        IntentFilter filter = new IntentFilter();
        filter.addAction(BasicInfo.ACTION_MY_LOCATION);
        filter.addAction(BasicInfo.ACTION_REQUEST_PERMISSIONS);
        getContext().registerReceiver(gpsReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();

        getContext().unregisterReceiver(gpsReceiver);

        Intent gpsIntent = new Intent(getContext(), GpsTrackService.class);
        getContext().stopService(gpsIntent);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onLowMemory();
//        mapView.onDestroy();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        String postKey = markerInfos.get(marker.getId());
        Log.d(TAG, "Selected a marker. post: " + postKey);

        Intent intent = new Intent(getContext(), TimelapseActivity.class);
        intent.putExtra(BasicInfo.EXTRA_POST_KEY, postKey);
        startActivity(intent);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (map != null) {
            map.clear();
            map = null;
        }
        map = googleMap;
        map.setOnInfoWindowClickListener(this);

        UiSettings uiSettings = map.getUiSettings();
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setZoomGesturesEnabled(true);
        uiSettings.setTiltGesturesEnabled(false);
        uiSettings.setRotateGesturesEnabled(false);

        LatLng latLngCenter = new LatLng(myLat, myLng);
        if (latLngCenter.longitude == 0 && latLngCenter.latitude == 0) {
            Toast.makeText(getContext(), "내 위치를 가져올 수 없습니다.", Toast.LENGTH_SHORT).show();
            Log.w(TAG, "cannot get my location ");
        }
        Log.i(TAG, "lat: " + latLngCenter.latitude + ", lng: " + latLngCenter.longitude);

        searchCircle = map.addCircle(new CircleOptions().center(latLngCenter).radius(1000));
        searchCircle.setFillColor(Color.argb(66, 255, 0, 255));
        searchCircle.setStrokeColor(Color.argb(66, 0, 0, 0));

        // 마커를 추가하고 카메라를 현재 위치로 이동시킨다
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngCenter, INITIAL_ZOOM_LEVEL));
        map.setOnCameraMoveListener(this);

        // GeoFire 설정
        GeoLocation myLocation = new GeoLocation(latLngCenter.latitude, latLngCenter.longitude);
        geoQuery = this.geoFire.queryAtLocation(myLocation, 1);
        geoQuery.addGeoQueryEventListener(this);

        // 마커 설정
        this.markers = new HashMap<>();
    }

    @Override
    public void onKeyEntered(final String key, final GeoLocation location) {
        Log.d(TAG, "entered key: " + key + ", location: (" + location.latitude + ", " + location.longitude + ")");

        rootRef.child("posts").child(key).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(final DataSnapshot postSnapshot) {
                final Post post = postSnapshot.getValue(Post.class);

                rootRef.child("users").child(post.uid).addListenerForSingleValueEvent(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot userSnapshot) {
                        User target = userSnapshot.getValue(User.class);

                        /*
                         * 다음 조건들을 모두 만족하는 포스트들만 지도에 표시한다.
                         * 1. 공개범위(privacy)를 만족
                         * 2. 내가 작성자를, 혹은 작성자가 나를 팔로우
                         */
                        String uid = FirebaseUtils.getUid();
                        boolean isMyPost = post.author.equals(uid);
                        boolean isPrivate = post.privacy.contains("private");
                        boolean following = target.followers.containsKey(uid);
                        boolean followed = target.followings.containsKey(uid);
                        if (map != null && markers != null
                                && isMyPost || (!isPrivate && (following || followed))){
                            Marker marker = map.addMarker(
                                    new MarkerOptions()
                                            .position(new LatLng(location.latitude, location.longitude))
                                            .title(post.title)
                                            .snippet(post.body));
                            marker.showInfoWindow();
                            markerInfos.put(marker.getId(), postSnapshot.getKey());

                            markers.put(key, marker);
                        }

//                        Glide.with(TimelapseSnsApplication.getContext()).load(Uri.parse(post.photoGroupUri + post.filenames.get(0) + "?alt=media"))
//                                .asBitmap().fitCenter().override(100, 100).into(new SimpleTarget<Bitmap>() {
//                            @Override
//                            public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
//                            }
//                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError err) {
                Log.w(TAG, "Could not fetch post data", err.toException());
            }
        });
    }

    @Override
    public void onKeyExited(String key) {
        Log.d(TAG, "exited key: " + key);
        Marker marker = markers.get(key);
        if (marker != null && markers != null) {
            marker.remove();
            markers.remove(key);
        }
    }

    @Override
    public void onKeyMoved(String key, GeoLocation location) {
        Log.d(TAG, "moved key: " + key + ", location: (" + location.latitude + ", " + location.longitude + ")");
        Marker marker = this.markers.get(key);
        if (marker != null) {
            this.animateMarkerTo(marker, location.latitude, location.longitude);
        }
    }

    @Override
    public void onGeoQueryReady() {

    }

    @Override
    public void onGeoQueryError(DatabaseError error) {
        new AlertDialog.Builder(getContext())
                .setTitle("Error")
                .setMessage("There was an unexpected error querying GeoFire: " + error.getMessage())
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onCameraMove() {
        CameraPosition cameraPosition = this.map.getCameraPosition();
        LatLng center = cameraPosition.target;
        double radius = zoomLevelToRadius(cameraPosition.zoom);
        this.searchCircle.setCenter(center);
        this.searchCircle.setRadius(radius);
        this.geoQuery.setCenter(new GeoLocation(center.latitude, center.longitude));
        // radius in km
        this.geoQuery.setRadius(radius / 1000);
    }

    protected void saveLocationToGeoFire(String key, final double lat, final double lng) {
        geoFire.setLocation(key, new GeoLocation(lat, lng), new GeoFire.CompletionListener() {

            @Override
            public void onComplete(String key, DatabaseError err) {
                if (err != null) {
                    Log.w(TAG, "There was an error saving the location to GeoFire", err.toException());
                    return;
                }
                Log.d(TAG, "Location saved. key: " + key + ", lat: " + lat + ", lng" + lng);
            }
        });
    }

    // Animation handler for old APIs without animation support
    private void animateMarkerTo(final Marker marker, final double lat, final double lng) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long DURATION_MS = 3000;
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final LatLng startPosition = marker.getPosition();
        handler.post(new Runnable() {
            @Override
            public void run() {
                float elapsed = SystemClock.uptimeMillis() - start;
                float t = elapsed / DURATION_MS;
                float v = interpolator.getInterpolation(t);

                double currentLat = (lat - startPosition.latitude) * v + startPosition.latitude;
                double currentLng = (lng - startPosition.longitude) * v + startPosition.longitude;
                marker.setPosition(new LatLng(currentLat, currentLng));

                // if animation is not finished yet, repeat
                if (t < 1) {
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    private double zoomLevelToRadius(double zoomLevel) {
        // Approximation to fit circle into view
        return 16384000 / Math.pow(2, zoomLevel);
    }
}
