package kr.ac.mju.cs.timelapsesns.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.TimelapseSnsApplication;
import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;
import kr.ac.mju.cs.timelapsesns.fragment.dialog.EditorDialogFragment;
import kr.ac.mju.cs.timelapsesns.model.Post;
import kr.ac.mju.cs.timelapsesns.service.storage.PhotoUploadService;
import kr.ac.mju.cs.timelapsesns.util.ImageUtils;
import kr.ac.mju.cs.timelapsesns.util.StringParser;

@SuppressWarnings("ConstantConditions")
public class TimelapseActivity extends BaseActivity {
    private static final String TAG = TimelapseActivity.class.getSimpleName();

    @BindView(R.id.image_pager)
    ViewPager imagePager;

    @BindView(R.id.fab_add_photo)
    FloatingActionButton fabAddPhoto;

    @BindView(R.id.time_spinner)
    Spinner timeSpinner;

    @BindView(R.id.timelapse_img)
    ImageView timelapseImg;

    @BindView(R.id.play_btn)
    ImageView playBtn;

    private Menu menu;

    private DatabaseReference rootRef;
    private DatabaseReference postRef;
    private DatabaseReference userPostRef;
    private DatabaseReference locationRef;
    private ValueEventListener postListener;

    private String uid;
    private String postKey;
    private boolean isAuthor;

    private BroadcastReceiver broadcastReceiver;
    private SingleImageAdapter imageAdapter;
    private ArrayList<Uri> fileUris;
    private ArrayList<Bitmap> bitmaps;

    private Handler handler = new Handler();
    private ImageThread thread;
    private boolean running = false;
    private String timeInterval;

    private boolean spinnerInitial = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timelapse);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        postKey = intent.getStringExtra(BasicInfo.EXTRA_POST_KEY);
        Log.d("postkey@", postKey);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        uid = auth.getCurrentUser().getUid();
        rootRef = FirebaseDatabase.getInstance().getReference();
        postRef = rootRef.child("posts").child(postKey);
        userPostRef = rootRef.child("user-posts").child(uid).child(postKey);
        locationRef = rootRef.child("_geofire").child(postKey);

        imageAdapter = new SingleImageAdapter(this);
        fileUris = new ArrayList<>();

        broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "onReceive: " + intent);

                switch (intent.getAction()) {
                    case BasicInfo.APPEND_UPLOAD_COMPLETED:
                        Log.d(TAG, "completed uploading");
                        onUploadResultIntent(intent);
                        break;
                    case BasicInfo.APPEND_UPLOAD_ERROR:
                        Log.e(TAG, "error while uploading");
                        break;
                }
            }
        };

        initSpinner();
    }

    @OnClick(R.id.fab_add_photo)
    public void onImageLoadClick() {
        ImageUtils.fetchImage(TimelapseActivity.this);
    }

    @Override
    public void onStart() {
        super.onStart();

        postListener = new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final Post post = dataSnapshot.getValue(Post.class);

                if(post != null) {
                    initImgThumb(Uri.parse(post.photoGroupUri + post.filenames.get(0) + "?alt=media"));
                    cleanImages();
                    for (String filename : post.filenames) {
                        Log.d("filenames:", filename);
                        Uri fileUri = Uri.parse(post.photoGroupUri + filename + "?alt=media");
                        fileUris.add(fileUri);
                        imageAdapter.addImage(fileUri);
                    }
                    convertUriToBitmap();
                    imagePager.setAdapter(imageAdapter);
                }
            }

            @Override
            public void onCancelled(DatabaseError err) {
                Log.w(TAG, "Could not fetch data from user-posts", err.toException());
            }
        };


        postRef.addValueEventListener(postListener);

        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.registerReceiver(broadcastReceiver, PhotoUploadService.getIntentFilter());
    }

    private void convertUriToBitmap() {
        bitmaps = new ArrayList<>();

        if(fileUris != null) {
            BitmapWorkerTask bitmapWorkerTask = new BitmapWorkerTask();
            bitmapWorkerTask.execute(fileUris);
        }
    }


    @Override
    public void onStop() {
        super.onStop();

        if (postListener != null)
            postRef.removeEventListener(postListener);

        stopImgPlay();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;

        // 글쓴이가 아니라면 접근을 제한한다
        userPostRef.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Post post = dataSnapshot.getValue(Post.class);
                if(post != null)
                    initOtherComponents();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return true;
    }

    private void initOtherComponents() {
        menu.add(Menu.NONE, R.id.activity_timelapse_itm_edit, Menu.NONE, getString(R.string.activity_timelapse_menu_edit));
        menu.add(Menu.NONE, R.id.activity_timelapse_itm_delete, Menu.NONE, getString(R.string.activity_timelapse_menu_delete));
        fabAddPhoto.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.activity_timelapse_itm_edit:
                EditorDialogFragment editorDialogFragment = EditorDialogFragment.newInstance(postKey);
                editorDialogFragment.show(getSupportFragmentManager(), "editor_dialog");
                break;
            case R.id.activity_timelapse_itm_delete:
                deletePost();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK)
            return;

        if (requestCode == BasicInfo.PICK_FROM_ALBUM
                || requestCode == BasicInfo.PICK_FROM_CAMERA) {
            if (resultCode != RESULT_OK) {
                switch (requestCode) {
                    case BasicInfo.PICK_FROM_ALBUM:
                        Toast.makeText(this, "앨범에서 사진을 가져오는 데 실패했습니다", Toast.LENGTH_SHORT).show();
                        break;
                    case BasicInfo.PICK_FROM_CAMERA:
                        Toast.makeText(this, "찍은 사진을 업로드하는 데 실패했습니다", Toast.LENGTH_SHORT).show();
                        break;
                }
                return;
            }


            Uri fileUri;
            if (requestCode == BasicInfo.PICK_FROM_CAMERA) {
                fileUri = ImageUtils.fileUri;
                if (fileUri == null)
                    return;
            } else {
                fileUri = data.getData();
                if (fileUri == null && ImageUtils.findImageFileNameFromUri(this, fileUri) == false)
                    return;
            }

            showProgressDialog();
            Log.d(TAG, "Upload photo to uri: " + fileUri.toString());
            startService(new Intent(TimelapseActivity.this, PhotoUploadService.class)
                    .putExtra(BasicInfo.EXTRA_UID, uid)
                    .putExtra(BasicInfo.EXTRA_POST_KEY, postKey)
                    .putExtra(BasicInfo.EXTRA_FILE_URI, fileUri)
                    .setAction(BasicInfo.ACTION_APPEND_UPLOAD));
        }
    }

    public void cleanImages() {
        fileUris.clear();
        imageAdapter.cleanImages();
    }

    /**
     * 업로드 결과를 처리한다
     *
     * @param intent 업로드 결과 Intent
     */
    private void onUploadResultIntent(Intent intent) {
        final Uri downloadUri = intent.getParcelableExtra(BasicInfo.EXTRA_DOWNLOAD_URL);

        postRef.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Post post = dataSnapshot.getValue(Post.class);
                post.filenames.add(StringParser.parsePhotoFilename(downloadUri));
                postRef.child("filenames").setValue(post.filenames);
                userPostRef.child("filenames").setValue(post.filenames);

                Log.i(TAG, "successfully saved uploaded file to post " + post.toString());
                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "failed to save uploaded file to post reference");
            }
        });
    }

    @OnClick({R.id.play})
    public void onPlayButtonClicked(View view) {
        if(!running) {
            playBtn.setVisibility(View.GONE);
            fabAddPhoto.setVisibility(View.GONE);
            if(bitmaps.size() > 0)
                startImgPlay();
        } else {
            playBtn.setVisibility(View.VISIBLE);
            fabAddPhoto.setVisibility(View.VISIBLE);
            stopImgPlay();
        }
    }

    private void initSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.times, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timeSpinner.setAdapter(adapter);
        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                if(spinnerInitial) {
                    timeInterval = adapterView.getItemAtPosition(pos) + "000";
                    spinnerInitial = false;
                } else {
                    timeInterval = adapterView.getItemAtPosition(pos) + "000";
                    if (!running)
                        playBtn.setVisibility(View.GONE);

                    stopImgPlay();
                    if(bitmaps.size() > 0)
                        startImgPlay();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initImgThumb(Uri thumbnailUri) {
        Glide.with(this)
                .load(thumbnailUri)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(timelapseImg);
    }

    private void startImgPlay() {
        thread = new ImageThread();
        thread.start();
    }

    private void stopImgPlay() {
        if (running) {
            running = false;
            try {
                thread.join();
                thread.interrupt();
            } catch (InterruptedException e) {
            }
        }
    }

    private class ImageThread extends Thread {
        int currentIndex = 0;

        @Override
        public void run() {
            running = true;
            while (running) {
//                synchronized (this) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            timelapseImg.setImageBitmap(bitmaps.get(currentIndex));
                            currentIndex++;
                            if (currentIndex == bitmaps.size())
                                currentIndex = 0;
                        }
                    });

                    try {
                        Thread.sleep(Long.parseLong(timeInterval));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
//            }
        }
    }

    private class SingleImageAdapter extends PagerAdapter {

        private ArrayList<Uri> photoUris;
        private LayoutInflater inflater;

        private SingleImageAdapter(Context context) {
            this.photoUris = new ArrayList<>();
            this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return photoUris.size();
        }

        @Override
        public float getPageWidth(int position) {
            return 0.34f;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View view = inflater.inflate(R.layout.simple_photo_view, container, false);
            final ImageView photo = view.findViewById(R.id.photo_image);
            photo.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    final AlertDialog.Builder deleteDialog = new AlertDialog.Builder(TimelapseActivity.this,
                            android.R.style.Theme_DeviceDefault_Light_Dialog);

                    deleteDialog.setTitle("사진을 삭제 하시겠습니까?")
                            .setPositiveButton("예", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    deleteItem(position);
                                }
                            })
                            .setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .setCancelable(true)
                            .show();
                    return false;
                }
            });

            Glide.with(getApplicationContext())
                    .load(photoUris.get(position))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(photo);

            container.addView(view);
            return view;
        }

        private void deleteItem(int position) {
            if(position == 0) {
                deletePost();
            } else {
                userPostRef.child("filenames").child(String.valueOf(position)).setValue(null);
                postRef.child("filenames").child(String.valueOf(position)).setValue(null);
                photoUris.remove(position);
                notifyDataSetChanged();
            }
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        private void addImage(Uri uri) {
            photoUris.add(uri);
            notifyDataSetChanged();
        }

        private void cleanImages() {
            photoUris.clear();
        }
    }

    private void deletePost() {
        postRef.setValue(null);
        userPostRef.setValue(null);
        locationRef.setValue(null);
        finish();
    }

    private class BitmapWorkerTask extends AsyncTask<ArrayList<Uri>, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(ArrayList<Uri>[] uris) {
            Bitmap bitmap;

            for(int i=0; i<uris[0].size(); i++) {
                try {
                    bitmap = Glide.with(TimelapseSnsApplication.getContext())
                            .load(uris[0].get(i))
                            .asBitmap()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(600, 600)
                            .get();
                    bitmaps.add(bitmap);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }
    }
}
