package kr.ac.mju.cs.timelapsesns.fragment;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import kr.ac.mju.cs.timelapsesns.util.FirebaseUtils;

public class MyPostsFragment extends PostListFragment {

    public MyPostsFragment() { }

    @Override
    public Query getQuery(DatabaseReference rootRef) {
        return rootRef.child("user-posts").child(FirebaseUtils.getUid());
    }
}
