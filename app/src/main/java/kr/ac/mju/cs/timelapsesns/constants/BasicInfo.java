package kr.ac.mju.cs.timelapsesns.constants;

/**
 * Created by Tak on 2017. 12. 26..
 */

public interface BasicInfo {
    /*
    ** Auth Provider
     */
    String ANONYMOUS = "anonymous";
    String FACEBOOK = "facebook";
    String EMAIL = "email";
    String LOGOUT = "LOGOUT";

    /*
    ** FCM Server Key
     */
    String FCM_SERVER_KEY = "AAAAEm2r3gc:APA91bFBG25zQ1_GysFhSfh0yRAzcIy3y3a3GKTQaKm-iIZx4f9rna_H24KBUAbF2subT7SYwa9MHoTqJhLtxJX7FlDxOjF51DFRc746PnhDhKy8PoDrznacjpXGk0YwwGf_9qUWuUu3";
    String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";

    /*
    ** Device
     */
    int PICK_FROM_CAMERA = 0;
    int PICK_FROM_ALBUM = 1;
    int CROP_FROM_CAMERA = 2;

    /*
    ** Intent Action
     */
    String ACTION_MY_LOCATION = "kr.ac.mju.cs.timelapsesns.action.my.location";
    String ACTION_REQUEST_PERMISSIONS = "kr.ac.mju.cs.timelapsesns.action.permissions";

    String ACTION_NEW_UPLOAD = "action_new_upload";
    String ACTION_APPEND_UPLOAD = "action_append_upload";
    String NEW_UPLOAD_COMPLETED = "new_upload_completed";
    String NEW_UPLOAD_ERROR = "new_upload_error";
    String APPEND_UPLOAD_COMPLETED = "append_upload_completed";
    String APPEND_UPLOAD_ERROR = "append_upload_error";


    /*
    ** Intent Extra
     */
    String EXTRA_UID = "extra_uid";
    String EXTRA_POST_KEY = "extra_post_key";
    String EXTRA_FILE_URI = "extra_file_uri";
    String EXTRA_DOWNLOAD_URL = "extra_download_url";

    String EXTRA_FIRE_DB_REF = "EXTRA_FIRE_DB_REF";
    String EXTRA_LAT_LOCATION = "EXTRA_LAT_LOCATION";
    String EXTRA_LNG_LOCATION = "EXTRA_LNG_LOCATION";

    String EXTRA_USER_LIST = "extra_user_list";

    String EXTRA_CLICKED_LAT = "extra_clicked_lat";
    String EXTRA_CLICKED_LNG = "extra_clicked_lng";

    String EXTRA_SHARE_USER = "extra_share_user";
    String EXTRA_AUTHOR = "extra_author";
    String EXTRA_TITLE = "extra_title";
    String EXTRA_BODY = "extra_body";
    String EXTRA_DOWNLOAD_URI = "extra_download_uri";


    /*
    ** Notification Id
     */
    int PROGRESS_NOTIFICATION_ID = 0;
    int FINISHED_NOTIFICATION_ID = 1;



    /*
    ** Permissions Request
     */
    int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;
    int PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
    int PERMISSIONS_REQUEST_CAMERA = 2;
    int PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 4;
    int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 5;

    int RC_PERMISSION = 1;



    /*
    ** Google Map
     */
    int INITIAL_ZOOM_LEVEL = 14;


    /*
    ** Geo Fire
     */
    String GEO_FIRE_DB = "https://timelapsesns.firebaseio.com";
    String GEO_FIRE_REF = GEO_FIRE_DB + "/_geofire";


    /*
    ** ViewHolder Type
     */
    int VIEW_TYPE_USER = 1000;
    int VIEW_TYPE_USER_SELECTION = 1001;
}





























