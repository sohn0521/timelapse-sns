package kr.ac.mju.cs.timelapsesns.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.json.JSONObject;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d("AlarmReceiver", "onReceive!");
        final ArrayList<String> fcmUserList = intent.getStringArrayListExtra(BasicInfo.EXTRA_SHARE_USER);

        //fcm 노티를 통해 보여줄 내용들
        final String author = intent.getStringExtra(BasicInfo.EXTRA_AUTHOR);    //본인
        final String title = intent.getStringExtra(BasicInfo.EXTRA_TITLE);
        final String body = intent.getStringExtra(BasicInfo.EXTRA_BODY);
        final String downloadUri = intent.getStringExtra(BasicInfo.EXTRA_DOWNLOAD_URI);
        final String postKey = intent.getStringExtra(BasicInfo.EXTRA_POST_KEY);
        Log.d("AlarmReceiver!", postKey);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for(int i=0; i<fcmUserList.size(); i++) {
                        // FMC 메시지 생성 start
                        JSONObject root = new JSONObject();
                        JSONObject data = new JSONObject();
                        data.put("author", author);
                        data.put("title", title);
                        data.put("body", body);
                        data.put("downloadUri", downloadUri);
                        data.put("postKey", postKey);
                        root.put("data", data);
                        root.put("to", fcmUserList.get(i));
                        // FMC 메시지 생성 end

                        URL Url = new URL(BasicInfo.FCM_MESSAGE_URL);
                        HttpURLConnection conn = (HttpURLConnection) Url.openConnection();
                        conn.setRequestMethod("POST");
                        conn.setDoOutput(true);
                        conn.setDoInput(true);
                        conn.addRequestProperty("Authorization", "key=" + BasicInfo.FCM_SERVER_KEY);
                        conn.setRequestProperty("Accept", "application/json");
                        conn.setRequestProperty("Content-type", "application/json");
                        OutputStream os = conn.getOutputStream();
                        os.write(root.toString().getBytes("utf-8"));
                        os.flush();
                        conn.getResponseCode();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }
}
