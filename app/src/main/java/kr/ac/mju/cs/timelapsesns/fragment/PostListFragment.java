package kr.ac.mju.cs.timelapsesns.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.activity.TimelapseActivity;
import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;
import kr.ac.mju.cs.timelapsesns.model.Post;
import kr.ac.mju.cs.timelapsesns.model.User;
import kr.ac.mju.cs.timelapsesns.util.FirebaseUtils;
import kr.ac.mju.cs.timelapsesns.viewholder.PostViewHolder;

public abstract class PostListFragment extends BaseFragment {

    private static final String TAG = PostListFragment.class.getSimpleName();

    private DatabaseReference rootRef;

    private FirebaseRecyclerAdapter<Post, PostViewHolder> adapter;
    private RecyclerView recycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_all_posts, container, false);

        rootRef = FirebaseDatabase.getInstance().getReference();

        recycler = rootView.findViewById(R.id.messages_list);
        recycler.setHasFixedSize(true);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        showProgressDialog();
        // LayoutManager를 역순으로 초기화한다
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recycler.setLayoutManager(layoutManager);

        Query postsQuery = getQuery(rootRef);
        FirebaseRecyclerOptions<Post> options = new FirebaseRecyclerOptions.Builder<Post>()
                .setQuery(postsQuery, Post.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<Post, PostViewHolder>(options) {

            @Override
            public PostViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
                LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
                return new PostViewHolder(inflater.inflate(R.layout.item_post, viewGroup, false));
            }

            @Override
            protected void onBindViewHolder(final PostViewHolder viewHolder, int position, final Post model) {
                final DatabaseReference postRef = getRef(position);

                // Set click listener for the whole post view
                final String postKey = postRef.getKey();
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Launch PostDetailActivity
                        Intent intent = new Intent(getActivity(), TimelapseActivity.class);
                        intent.putExtra(BasicInfo.EXTRA_POST_KEY, postKey);
                        startActivity(intent);
                    }
                });

                rootRef.child("users").child(FirebaseUtils.getUid()).addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        if (user != null && user.followings.containsKey(model.uid)) {
                            viewHolder.followView.setImageResource(R.drawable.ic_person_black_24dp);
                        } else {
                            viewHolder.followView.setImageResource(R.drawable.ic_person_outline_black_24dp);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError err) {
                        Log.w(TAG, "Couldn't fetch user data", err.toException());
                    }
                });

                if (model.likedBy == null) {
                    model.likedBy = new HashMap<>();
                }

                if (model.likedBy.containsKey(FirebaseUtils.getUid())) {
                    viewHolder.likeView.setImageResource(R.drawable.ic_favorite_black_24dp);
                } else {
                    viewHolder.likeView.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                }

                // Bind Post to ViewHolder
                viewHolder.bind(model, new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        onFollowClicked(FirebaseUtils.getUid(), model.uid);
                    }
                }, new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        DatabaseReference globalPostRef = rootRef.child("posts").child(postRef.getKey());
                        DatabaseReference userPostRef = rootRef.child("user-posts").child(model.uid).child(postRef.getKey());

                        onLikeClicked(globalPostRef);
                        onLikeClicked(userPostRef);
                    }
                });
            }
        };
        recycler.setAdapter(adapter);
        hideProgressDialog();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (adapter != null) {
            adapter.startListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (adapter != null) {
            adapter.stopListening();
        }
    }

    public abstract Query getQuery(DatabaseReference databaseReference);

    private void onFollowClicked(final String fromUid, final String toUid) {

        if (TextUtils.isEmpty(fromUid) || TextUtils.isEmpty(toUid) || fromUid.equals(toUid)) {
            return;
        }

        rootRef.child("users").child(fromUid).runTransaction(new Transaction.Handler() {

            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                User from = mutableData.getValue(User.class);
                if (from == null) {
                    return Transaction.success(mutableData);
                }

                if (from.followings.containsKey(toUid)) {
                    from.followingCount--;
                    from.followings.remove(toUid);
                } else {
                    from.followingCount++;
                    from.followings.put(toUid, true);
                }

                mutableData.setValue(from);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });
        rootRef.child("users").child(toUid).runTransaction(new Transaction.Handler() {

            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                User to = mutableData.getValue(User.class);
                if (to == null) {
                    return Transaction.success(mutableData);
                }

                if (to.followers.containsKey(fromUid)) {
                    to.followerCount--;
                    to.followers.remove(fromUid);
                } else {
                    to.followerCount++;
                    to.followers.put(fromUid, true);
                }

                mutableData.setValue(to);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });
    }

    private void onLikeClicked(DatabaseReference postRef) {
        postRef.runTransaction(new Transaction.Handler() {

            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Post p = mutableData.getValue(Post.class);
                if (p == null) {
                    return Transaction.success(mutableData);
                }

                if (p.likedBy.containsKey(FirebaseUtils.getUid())) {
                    p.likeCount--;
                    p.likedBy.remove(FirebaseUtils.getUid());
                } else {
                    p.likeCount++;
                    p.likedBy.put(FirebaseUtils.getUid(), true);
                }

                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError err, boolean b, DataSnapshot dataSnapshot) {
                Log.d(TAG, "postTransaction::onComplete: " + err);
            }
        });
    }
}
