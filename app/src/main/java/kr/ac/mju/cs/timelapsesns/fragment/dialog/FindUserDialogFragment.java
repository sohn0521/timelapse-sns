package kr.ac.mju.cs.timelapsesns.fragment.dialog;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.TimelapseSnsApplication;
import kr.ac.mju.cs.timelapsesns.adapter.UserAdapter;
import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;
import kr.ac.mju.cs.timelapsesns.fragment.BaseFragment;
import kr.ac.mju.cs.timelapsesns.model.ParcelableUser;
import kr.ac.mju.cs.timelapsesns.util.SoundSearchUtil;

/**
 * Created by Tak on 2018. 1. 8..
 */


public class FindUserDialogFragment extends BaseFragment {

//    private static final String EXTRA_USER_LIST = "extra_user_list";

    @BindView(R.id.user_selection_list)
    RecyclerView userSelectionRecycler;

    @BindView(R.id.user_list)
    RecyclerView findUserRecycler;

    @BindView(R.id.search_user_edit)
    EditText findUserEdit;

    private OnAdapterItemClickLIstener listener;
    private boolean isSelected;
    private int mPosition;

    //전체 유저리스트
    private ArrayList<ParcelableUser> allUserList;

    //탐색을 통해 나온 유저리스트
    private ArrayList<ParcelableUser> searchUserList;
    private UserAdapter userAdapter;

    //선택된 유저리스트
    private ArrayList<ParcelableUser> selectedUserList;
    private UserAdapter selectedUserAdapter;

    public FindUserDialogFragment() {}
    public static FindUserDialogFragment newInstance(ArrayList<ParcelableUser> userList) {
        FindUserDialogFragment findUserDialog= new FindUserDialogFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(BasicInfo.EXTRA_USER_LIST, userList);
        findUserDialog.setArguments(args);
        return findUserDialog;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            allUserList = getArguments().getParcelableArrayList(BasicInfo.EXTRA_USER_LIST);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_find_user, container, false);
        ButterKnife.bind(this, view);

        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(findUserEdit, InputMethodManager.SHOW_IMPLICIT);
        findUserEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    sendEditDialogWithMessage();
                    return true;
                }
                return false;
            }
        });

        selectedUserList = new ArrayList<>();

        //유저 탐색 목록에 대한 RecyclerView
        findUserRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        findUserRecycler.setHasFixedSize(true);

        userAdapter = new UserAdapter(getContext(), BasicInfo.VIEW_TYPE_USER);
        userAdapter.setOnUserItemClickListener(new UserAdapter.OnUserItemClickListener() {
            @Override
            public void onUserClicked(View view, ParcelableUser user, int position) {
                //TODO: Set 자료구조로 바꿀 것.
                Log.d("find_user", user.username);
                selectedUserList.add(user);
                selectedUserAdapter.addUserName(user.username);
            }

            @Override
            public void onUserNameClicked(View view, String username, int position) {
                selectedUserList.remove(position);
            }
        });
        findUserRecycler.setAdapter(userAdapter);


        //선택된 유저 목록에 대한 RecyclerView
        userSelectionRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        userSelectionRecycler.setHasFixedSize(true);

        selectedUserAdapter = new UserAdapter(getContext(), BasicInfo.VIEW_TYPE_USER_SELECTION);
        userSelectionRecycler.setAdapter(selectedUserAdapter);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        searchUserList = new ArrayList<>();
        findUserEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                userAdapter.clear();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                String targetUser = charSequence.toString();

                if(targetUser.length() == 0) {
                    Log.d("user!!", "ㅁㄴㅇㄹ");
                    findUserRecycler.setVisibility(View.INVISIBLE);
                } else {
                    findUserRecycler.setVisibility(View.VISIBLE);
                    searchUserList.clear();
                    for(int i=0; i<allUserList.size(); i++) {
                        String user = allUserList.get(i).username;

                        //초성검색으로 찾는 대상이 있으면 list에 넣어준다.
                        if(SoundSearchUtil.matchString(user, targetUser) == true) {
                            searchUserList.add(allUserList.get(i));
                            Log.d("user!!", user);
                        }
                    }
                    userAdapter.addUser(searchUserList);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @OnClick(R.id.share_finish)
    public void finishButtonClicked() {
        sendEditDialogWithMessage();
    }

    private void sendEditDialogWithMessage() {
        if(selectedUserList.size() > 0) {
            listener.onAdapterItemClick(selectedUserList);
            Toast.makeText(TimelapseSnsApplication.getContext(), "공유 설정 완료", Toast.LENGTH_SHORT).show();
        }

        dismiss();
    }


    public interface OnAdapterItemClickLIstener {
        void onAdapterItemClick(ArrayList<ParcelableUser> selectedUserList);
    }

    public void setOnAdapterItemClickListener(OnAdapterItemClickLIstener listener) {
        this.listener = listener;
    }

}
