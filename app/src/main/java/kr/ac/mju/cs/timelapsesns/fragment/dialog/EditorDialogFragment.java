package kr.ac.mju.cs.timelapsesns.fragment.dialog;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.core.GeoHash;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.ac.mju.cs.timelapsesns.R;
import kr.ac.mju.cs.timelapsesns.TimelapseSnsApplication;
import kr.ac.mju.cs.timelapsesns.adapter.UserAdapter;
import kr.ac.mju.cs.timelapsesns.constants.BasicInfo;
import kr.ac.mju.cs.timelapsesns.fragment.BaseFragment;
import kr.ac.mju.cs.timelapsesns.model.ParcelableUser;
import kr.ac.mju.cs.timelapsesns.model.Post;
import kr.ac.mju.cs.timelapsesns.model.User;
import kr.ac.mju.cs.timelapsesns.receiver.AlarmReceiver;
import kr.ac.mju.cs.timelapsesns.service.storage.PhotoUploadService;
import kr.ac.mju.cs.timelapsesns.util.SharedPreferencesUtil;
import kr.ac.mju.cs.timelapsesns.util.StringParser;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditorDialogFragment extends BaseFragment {

    private static final String TAG = "EditorDialogFragment";

    @BindView(R.id.new_post_title)
    EditText etTitle;

    @BindView(R.id.new_post_body)
    EditText etBody;

    @BindView(R.id.new_post_privacy_spin)
    Spinner spinnerPrivacy;

    @BindView(R.id.new_post_submit)
    FloatingActionButton fabSubmit;

    @BindView(R.id.user_selection_list)
    RecyclerView searchSelectionRecycler;

    @BindView(R.id.alarm_date)
    EditText alarmDate;

    private boolean isNewPost = true;
    private String date;
    private String mPostKey;
    private String myFcmToken;
    private String uid;
    private String author;
    private String title;
    private String body;
    private Uri downloadUri;
    private List<String> privacy;
    private String etPrivacy;
    private String photoGroupUri;
    private List<String> filenames;
    private UserAdapter selectedUserAdapter;

    private ArrayList<ParcelableUser> allUserList;        //이름이 있는 전체 유저리스트
    private List<String> mSelectedUserFCMs;          //그 중 선택된 유저의 fcm token
    private List<String> mSelectedUserNames;          //그 중 선택된 유저의 name

    private OnAdapterItemClickLIstener listener;

    private int mYear = -1;
    private int mMonth = -1;
    private int mDay = -1;
    private int mHourOfDay = -1;
    private int mMinute = -1;

    private Uri mFileUri;
    private Double mLatitude;
    private Double mLongtitude;

    private DatabaseReference rootRef;
    private DatabaseReference userRef;
    private ChildEventListener mChildEventListener;

    private BroadcastReceiver broadcastReceiver;

    public EditorDialogFragment() {}

    public static EditorDialogFragment newInstance(Uri fileUri, double latitude, double longitude) {
        EditorDialogFragment editorDialogFragment = new EditorDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(BasicInfo.EXTRA_FILE_URI, fileUri);
        args.putDouble(BasicInfo.EXTRA_CLICKED_LAT, latitude);
        args.putDouble(BasicInfo.EXTRA_CLICKED_LNG, longitude);
        editorDialogFragment.setArguments(args);
        return editorDialogFragment;
    }

    public static EditorDialogFragment newInstance(String postKey) {
        EditorDialogFragment editorDialogFragment = new EditorDialogFragment();
        Bundle args = new Bundle();
        args.putString(BasicInfo.EXTRA_POST_KEY, postKey);
        editorDialogFragment.setArguments(args);
        return editorDialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mFileUri = getArguments().getParcelable(BasicInfo.EXTRA_FILE_URI);
            mLatitude = getArguments().getDouble(BasicInfo.EXTRA_CLICKED_LAT);
            mLongtitude = getArguments().getDouble(BasicInfo.EXTRA_CLICKED_LNG);
            mPostKey = getArguments().getString(BasicInfo.EXTRA_POST_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_editor, container, false);
        ButterKnife.bind(this, view);

        uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        rootRef = FirebaseDatabase.getInstance().getReference();
        userRef = rootRef.child("users");

        //선택된 유저 목록에 대한 RecyclerView
        searchSelectionRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        searchSelectionRecycler.setHasFixedSize(true);

        selectedUserAdapter = new UserAdapter(getContext(), BasicInfo.VIEW_TYPE_USER_SELECTION);
        searchSelectionRecycler.setAdapter(selectedUserAdapter);

        mSelectedUserFCMs = new ArrayList<>();
        mSelectedUserNames = new ArrayList<>();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setSpinner();
        etBody.setSelection(etBody.length());
        //글 수정
        if (!TextUtils.isEmpty(mPostKey)) {
            isNewPost = false;
            readExistingPost();
        }

        broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "onReceive : " + intent);
                hideProgressDialog();

                switch (intent.getAction()) {
                    case BasicInfo.NEW_UPLOAD_COMPLETED:
                        Log.d(TAG, "completed uploading");
                        onUploadResultIntent(intent);
                        break;
                    case BasicInfo.NEW_UPLOAD_ERROR:
                        Log.e(TAG, "error while uploading");
                        break;
                }
            }
        };
    }

    private void readExistingPost() {
        rootRef.child("posts").child(mPostKey).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Post post = dataSnapshot.getValue(Post.class);

                author = post.author;
                photoGroupUri = post.photoGroupUri;
                filenames = post.filenames;
                mSelectedUserFCMs = post.selectedUserFCMs;
                mSelectedUserNames = post.selectedUserNames;
                mYear = post.year;
                mMonth = post.month;
                mDay = post.day;
                mHourOfDay = post.hourOfDay;
                mMinute = post.minute;

                etTitle.setText(post.title);
                etBody.setText(post.body);

                if (mSelectedUserNames != null)
                    for(int i=0; i<mSelectedUserNames.size(); i++)
                        selectedUserAdapter.addUserName(mSelectedUserNames.get(i));

                if(checkTimeSetting(mYear, mMonth, mDay, mHourOfDay, mMinute)) {
                    int m = mMonth + 1;
                    String date = mYear + "년 " + m + "월 " + mDay + "일 " + mHourOfDay + "시" + mMinute + "분";
                    alarmDate.setText(date);
                }

                if(post.privacy.get(0).equals("public")) {
                    spinnerPrivacy.setSelection(0);
                } else {
                    spinnerPrivacy.setSelection(1);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.privacy, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPrivacy.setAdapter(adapter);
        spinnerPrivacy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                String tempStr = adapterView.getItemAtPosition(pos).toString();
                if(tempStr.equals("공개")) {
                    etPrivacy = "public";
                } else {
                    etPrivacy = "privacy";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                etPrivacy = "public";
            }
        });
    }

    @OnClick({R.id.alarm_layout, R.id.alarm_date, R.id.new_post_submit, R.id.share_edit})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.alarm_layout:
            case R.id.alarm_date:
                setTime();
                break;
            case R.id.new_post_submit:
                submitPost();
                break;
            case R.id.share_edit:
                findSharingUser();
                break;
        }
    }

    private void findSharingUser() {
        //검색할 유저리스트를 받아왔는지 체크한다.
        FindUserDialogFragment findUserDialog = FindUserDialogFragment.newInstance(allUserList);
        findUserDialog.setOnAdapterItemClickListener(new FindUserDialogFragment.OnAdapterItemClickLIstener() {
            @Override
            public void onAdapterItemClick(ArrayList<ParcelableUser> selectedUserList) {
                String myFcmToken = SharedPreferencesUtil.getInstance().getFcmTokenKey();

                for(int i=0; i<selectedUserList.size(); i++) {
                    if(!selectedUserList.get(i).fcmToken.equals(myFcmToken))
                        mSelectedUserFCMs.add(selectedUserList.get(i).fcmToken);

                    String username = selectedUserList.get(i).username;
                    mSelectedUserNames.add(username);
                    selectedUserAdapter.addUserName(username);
                }
            }
        });
        findUserDialog.show(getFragmentManager(), "findUserDialog");
    }

    /**
     * 나중에 시간이 되면 푸쉬 알람을 받아 볼 수 있도록 설정.
     */
    private void setTime() {
        DatePickerFragment dateDialog = new DatePickerFragment();
        dateDialog.setOnAdapterItemClickListener(new DatePickerFragment.OnAdapterItemClickLIstener() {
            @Override
            public void onAdapterItemClick(final int year, final int month, final int day) {
                TimePickerFragment timeDialog = new TimePickerFragment();
                timeDialog.setOnAdapterItemClickListener(new TimePickerFragment.OnAdapterItemClickLIstener() {
                    @Override
                    public void onAdapterItemClick(int hourOfDay, int minute) {
                        mYear = year;
                        mMonth = month;
                        mDay = day;
                        mHourOfDay = hourOfDay;
                        mMinute = minute;

                        date = mYear + "년 " + month + 1 + "월 " + mDay + "일 " + mHourOfDay + "시" + mMinute + "분";
                        alarmDate.setText(date);

                    }
                });
                timeDialog.show(getFragmentManager(), "timePicker");
            }
        });
        dateDialog.show(getFragmentManager(), "datePicker");
    }

    /**
     * 작성한 포스트를 서버에 제출한다.
     */
    private void submitPost() {
        showProgressDialog("로딩중");
        title = etTitle.getText().toString();
        body = etBody.getText().toString();
        privacy = new ArrayList<>();
        privacy.add(etPrivacy);

        if (TextUtils.isEmpty(title)) {
            etTitle.setError(getString(R.string.required));
            hideProgressDialog();
            return;
        }
        if (TextUtils.isEmpty(body)) {
            etBody.setError(getString(R.string.required));
            hideProgressDialog();
            return;
        }
        if(mSelectedUserFCMs != null && mSelectedUserFCMs.size() > 0) {
            if(checkTimeSetting(mYear, mMonth, mDay, mHourOfDay, mMinute) == false) {
                alarmDate.setError(getString(R.string.alarm_required));
                hideProgressDialog();
                return;
            }
        }

        setEditingEnabled(false);
        if (isNewPost == true) {
            Log.d(TAG, "Upload photo to uri: " + mFileUri.toString());
            mPostKey = rootRef.child("posts").push().getKey();
            getContext().startService(new Intent(getContext(), PhotoUploadService.class)
                    .putExtra(BasicInfo.EXTRA_UID, uid)
                    .putExtra(BasicInfo.EXTRA_POST_KEY, mPostKey)
                    .putExtra(BasicInfo.EXTRA_FILE_URI, mFileUri)
                    .setAction(BasicInfo.ACTION_NEW_UPLOAD));
        }
        else {  // 글 수정
            writePost(author, title, body, privacy, photoGroupUri, filenames,
                    mSelectedUserNames, mSelectedUserFCMs, mYear, mMonth, mDay, mHourOfDay, mMinute);

            setEditingEnabled(true);
            closeDialog();
        }
    }

    /**
     * 업로드 결과를 처리한다
     *
     * @param intent 업로드 결과 Intent
     */
    private void onUploadResultIntent(Intent intent) {
        downloadUri = intent.getParcelableExtra(BasicInfo.EXTRA_DOWNLOAD_URL);
        Log.i(TAG, "Photo upload completed. downloadUri: " + downloadUri.toString());

        rootRef.child("users").child(uid).addListenerForSingleValueEvent(
                new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);

                        // 사용자를 가져오지 못한 경우 오류 처리
                        if (user == null) {
                            Log.e(TAG, "User " + uid + " is unexpectedly null");
                            Toast.makeText(getContext(), "사용자 정보를 가져오지 못했습니다", Toast.LENGTH_SHORT).show();
                            dismiss();
                            return;
                        }

                        myFcmToken = user.fcmToken;
                        author = user.username;
                        if (TextUtils.isEmpty(author)) {
                            author = user.uid;
                        }

                        // 시간이 설정 되어있다면, 알람 설정.
                        if(checkTimeSetting(mYear, mMonth, mDay, mHourOfDay, mMinute) == true)
                            setAlarmWithTime(mYear, mMonth, mDay, mHourOfDay, mMinute);

                        // 새 포스트 작성
                        writePost(author, title, body, privacy, downloadUri,
                                mSelectedUserNames, mSelectedUserFCMs, mYear, mMonth, mDay, mHourOfDay, mMinute);
                        writeNewGeoFire();

                        // 현재 액티비티 종료
                        setEditingEnabled(true);
                        closeDialog();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "Exception occurred while feching user info", databaseError.toException());
                        Toast.makeText(getContext(), "사용자 정보를 가져오는 도중 오류가 발생했습니다", Toast.LENGTH_SHORT).show();
                        setEditingEnabled(true);
                    }
                });
    }

    private boolean checkTimeSetting(int year, int month, int day, int hourOfDay, int minute) {
        if(year != -1 && month != -1 && day != -1 && hourOfDay != -1 && minute != -1)
            return true;

        return false;
    }

    /**
     * 알람 설정.
     * 설정한 시간 뒤에 공유설정한 사람들과 함께 '노티'를 통해 해당 포스트의 내용을 띄워줌.
     *
     * @param year
     * @param month
     * @param day
     * @param hourOfDay
     * @param minute
     */
    private void setAlarmWithTime(int year, int month, int day, int hourOfDay, int minute) {
        mSelectedUserFCMs.add(SharedPreferencesUtil.getInstance().getFcmTokenKey()); //자기 fcmToken

        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getContext(), AlarmReceiver.class);

        intent.putStringArrayListExtra(BasicInfo.EXTRA_SHARE_USER, (ArrayList<String>) mSelectedUserFCMs);

        //fcm 노티를 통해 보여줄 내용들 및 띄워줄 TimelapseActivity 정보
        intent.putExtra(BasicInfo.EXTRA_AUTHOR, author);
        intent.putExtra(BasicInfo.EXTRA_TITLE, title);
        intent.putExtra(BasicInfo.EXTRA_BODY, body);
        intent.putExtra(BasicInfo.EXTRA_DOWNLOAD_URI, downloadUri.toString());
        intent.putExtra(BasicInfo.EXTRA_POST_KEY, mPostKey);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), 0, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    /**
     * 새 포스트 편집창을 활성화하거나 비활성화한다.
     *
     * @param enabled 활성화 여부
     */
    private void setEditingEnabled(boolean enabled) {
        etTitle.setEnabled(enabled);
        etBody.setEnabled(enabled);
        spinnerPrivacy.setEnabled(enabled);
        if (enabled) {
            fabSubmit.setVisibility(View.VISIBLE);
        } else {
            fabSubmit.setVisibility(View.GONE);
        }
    }


    private void closeDialog() {
        Toast.makeText(TimelapseSnsApplication.getContext(), "성공적으로 등록하였습니다.", Toast.LENGTH_SHORT).show();
        hideProgressDialog();
        dismiss();
        if(listener != null)
            listener.onAdapterItemClick("success");
    }

    @Override
    public void onStart() {
        super.onStart();

        getAllUser();
        getPostContent();

        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getContext());
        broadcastManager.registerReceiver(broadcastReceiver, PhotoUploadService.getIntentFilter());
    }

    private void getAllUser() {
        allUserList = new ArrayList<>();
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                User user = dataSnapshot.getValue(User.class);

                if(user.username != null && Pattern.matches("^[ㄱ-ㅎ가-힣]*$", user.username)) {
                    Log.d(TAG, "onChildAdded!!" + user.username);
                    ParcelableUser userParcelable = new ParcelableUser(user.username, user.uid, user.fcmToken, false);
                    allUserList.add(userParcelable);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        userRef.addChildEventListener(childEventListener);
        mChildEventListener = childEventListener;
    }

    private void getPostContent() {
        String title = SharedPreferencesUtil.getInstance().getTitle();
        String body = SharedPreferencesUtil.getInstance().getBody();
        String date = SharedPreferencesUtil.getInstance().getDate();

        setPostContent(title, body, date);
    }

    private void setPostContent(String title, String body, String date) {
        etTitle.setText(title);
        etBody.setText(body);
        alarmDate.setText(date);
    }

    @Override
    public void onStop() {
        super.onStop();

        savePostContent();

        userRef.removeEventListener(mChildEventListener);
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferencesUtil.getInstance().clearPostContent();
    }

    private void savePostContent() {
        SharedPreferencesUtil.getInstance().setTitle(etTitle.getText().toString());
        SharedPreferencesUtil.getInstance().setBody(etBody.getText().toString());
        SharedPreferencesUtil.getInstance().setDate(date);
    }

    /**
     * 새 포스트를 다음 두 uri에 동시에 추가한다.
     * /posts/$postid
     * /user-posts/$uid/postid
     *
     * @param username 계정명
     * @param title 포스트 제목
     * @param body 포스트 설명
     * @param privacy 포스트 공개범위 (문자열 리스트)
     * @param selectedUserFCMs
     * @param selectedUserNames
     * @param year
     * @param month
     * @param day
     * @param hourOfDay
     * @param minute
     */
    private void writePost(String username, String title, String body, List<String> privacy, Uri downloadUri,
                           List<String> selectedUserNames, List<String> selectedUserFCMs, int year, int month, int day, int hourOfDay, int minute) {
        // 파일의 메타데이터를 파싱한다
        String photoGroupUri = StringParser.parsePhotoGroupUri(downloadUri);
        List<String> filenames = new ArrayList<>();
        filenames.add(StringParser.parsePhotoFilename(downloadUri));

        writePost(username, title, body, privacy, photoGroupUri, filenames,
                selectedUserNames, selectedUserFCMs, year, month, day, hourOfDay, minute);
    }

    private void writePost(String username, String title, String body, List<String> privacy, String photoGroupUri, List<String> filenames,
                            List<String> selectedUserNames, List<String> selectedUserFCMs, int year, int month, int day, int hourOfDay, int minute) {
        Log.i(TAG, "Write post with id: " + mPostKey + ", uid: " + uid);

        // Post를 DB에 입력한다
        Post post = new Post(uid, username, title, body, privacy, photoGroupUri, filenames, 1,
                selectedUserNames, selectedUserFCMs, year, month, day, hourOfDay, minute);
        Map<String, Object> postValues = post.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + mPostKey, postValues);
        childUpdates.put("/user-posts/" + uid + "/" + mPostKey, postValues);

        rootRef.updateChildren(childUpdates);
    }

    /**
     * 저장할 포스트 위치를 GeoFire 모델이랑 디비에도 저장함.
     */
    private void writeNewGeoFire() {
        Log.i(TAG, "Add new location with postKey: " + mPostKey);

        DatabaseReference keyRef = FirebaseDatabase.getInstance().getReference().child("_geofire").child(mPostKey);
        GeoLocation location = new GeoLocation(mLatitude, mLongtitude);
        GeoHash geoHash = new GeoHash(location);
        Map<String, Object> updates = new HashMap<>();
        updates.put("g", geoHash.getGeoHashString());
        updates.put("l", Arrays.asList(location.latitude, location.longitude));

        keyRef.setValue(updates, geoHash.getGeoHashString());
    }

    public interface OnAdapterItemClickLIstener {
        public void onAdapterItemClick(String finish);
    }

    public void setOnAdapterItemClickListener(OnAdapterItemClickLIstener listener) {
        this.listener = listener;
    }
}
